/* zterm.h - system module interface for Infuse */

#ifndef ZTERM_SEEN
#define ZTERM_SEEN

enum {
  /* fonts */
  Z_FONT1 = 1, Z_FONT2 = 2, Z_FONT3 = 3, Z_FONT4 = 4,

  /* styles */
  Z_ROMAN = 0, Z_REVERSE = 1, Z_BOLD = 2, Z_ITALIC = 4, Z_FIXED = 8,

  /* colours */
  Z_BLACK = 2, Z_RED = 3, Z_GREEN = 4, Z_YELLOW = 5,
  Z_BLUE = 6, Z_MAGENTA = 7, Z_CYAN = 8, Z_WHITE = 9,

  /* text alignment (aka justification) */
  Z_LEFT = 1, Z_RIGHT = 2, Z_CENTER = 3, Z_FULL = 4,

  /* special key symbols */
  Z_BS = 8, /* the form in BUREAUCRACY needs this */
  Z_CR = 13, /* dto. Note that 10 will NOT work. */
  Z_ESC = 27,
  Z_CRSR_UP = 129, Z_CRSR_DOWN = 130, Z_CRSR_LEFT = 131, Z_CRSR_RIGHT = 132,
#define Z_F(n) 132 + n
  Z_F1 = Z_F(1), Z_F2 = Z_F(2), Z_F3 = Z_F(3), Z_F4 = Z_F(4), Z_F5 = Z_F(5),
  Z_F6 = Z_F(6), Z_F7 = Z_F(7), Z_F8 = Z_F(8), Z_F9 = Z_F(9), Z_F10 = Z_F(10),
  Z_F11 = Z_F(11), Z_F12 = Z_F(12),
#undef Z_F
#define Z_KP(n) 145 + n
  Z_KP0 = Z_KP(0), Z_KP1 = Z_KP(1), Z_KP2 = Z_KP(2), Z_KP3 = Z_KP(3),
  Z_KP4 = Z_KP(4), Z_KP5 = Z_KP(5), Z_KP6 = Z_KP(6), Z_KP7 = Z_KP(7),
  Z_KP8 = Z_KP(8), Z_KP9 = Z_KP(9),
#undef Z_KP
  Z_MENU_CLICK = 252, Z_DOUBLE_CLICK = 253, Z_SINGLE_CLICK = 254
};


struct z_glyph {
  wchar_t ch;           /* Unicode character */
  unsigned short font;  /* 4 fonts in [1, 4] */
  unsigned short style; /* style bitmap */
  unsigned short fg;    /* foreground colour in range [2, 9] */
  unsigned short bg;    /* background colour in range [2, 9] */
};

typedef struct z_glyph z_glyph;

extern char *zbanne; /* machine-dependent part of banner */
extern int ztever;  /* version number of this z-term */
extern int zw_inu;  /* terminal width in units */
extern int zh_inu;  /* terminal height in units */
extern int zmwinu;  /* monospace font width in units */
extern int zfhinu;  /* font height in units */
extern int zhassn;  /* flag: sound effects possible */
extern int zhaspi;  /* flag: pix didplay possible */
extern int zhasco;  /* flag: colours available */
extern int zhasmo;  /* flag: mouse available */
extern int zhasmn;  /* flag: menus available */
extern int zhastm;  /* flag: timed input available */
extern int zf1pro;  /* flag: font 1 is proportionally spaced */
extern int zhasf2;  /* flag: font 2 available */
extern int zhasf3;  /* flag: font 3 available */
extern int zhasf4;  /* flag: font 4 available */
extern int zstyle;  /* bitmap of available text styles */

/* unicode translation table in core */

extern wchar_t cunitt[97]; /* for zscii 155..251 on input */

/* init and exit */
void z_init (int *pargc, char *argv[]);
void z_exit (void);

/* scroll region */
void zsescr (int top, int bot); /* set scroll region */
void zscrol (int n);            /* scroll up n units in scroll region */

/* box & pix ops */
void zcpbox (int x, int y, int w, int h, int x1, int y1);
void zshbox (int x, int y, int w, int h, int c); /* fill box with color */
void zshpix (int x, int y, int w, int h, void *img); /* show picture */

/* text cursor */
void zshcur (int x, int y, int fg, int bg); /* show cursor at position */
void zhdcur (void);                         /* hide last cursor */

/* text display */
void zshstr (int x, int y, int w, int align, z_glyph *s, int n);

/* font metrics */
int zstrwd (z_glyph *s, int n); 

/* keyboard input */
int zrdchr (long *wait_ms);

/* up-calls */
void (*zucmou) (int x, int y, int b, int m, int e); /* mouse up-call */

/* kludges */
void zflush (void);

#endif

