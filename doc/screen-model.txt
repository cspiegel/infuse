1 The Standard (ZIP, V3) screen model.

1.1 Text fonts and styles.

1.1.1 There is 1 font.

1.1.2 There are three text styles: ROMAN, REVERSE, and FIXED.  ROMAN
may be the same as FIXED, but FIXED must have a fixed pitch.  At least
40 FIXED characters should fit on a line.

1.1.3 Bit 6 of FLAGS-1 can be tested to figure out whether ROMAN is a
variable-pitch style.  If that bit is clear, then ROMAN and FIXED are
both fixed-pitch, though they may still have different appearance on
screen.

1.1.4 Text display in windows 0 and 1 can be in either ROMAN or FIXED,
and a game can switch bewteen the two by clearing or setting bit 1 of
FLAGS-2 of the header. REVERSE is used only on the status line, and it
is the only style availabe for the status line, and that can't be
changed. Since the status line is managed by the interpreter, it may
be displayed in fixed or variable pitch.

1.2 Buffering.

1.2.1 Buffering is always on in both windows, and there is no way to
turn it off. The two windows share a buffer, so the game has to take
care of flushing it before switching windows. (By printing a
new-line).

1.3 Scrolling.

1.3.1 Scrolling is always off in window 1 (upper), and always on in
window 0 (lower).  Text that does not fit into the upper window is
simply not printed.  The cursor stays at the lower right-hand corner
in this case.  In the lower window, the customany [MORE] prompt is
printed.

1.4 Windowing.

1.4.1 There are two windows, numbered 0 and 1, and there may be a
status line, which doesn't have a window number.

1.4.2 The windows and status line are ordered in this way, from top to
bottom: Status line (if available), window 1 (if available), window 0.

1.4.3 Status line availability is flagged by the interpreter by
clearing bit 4 of FLAGS-1 in the header.  (If that bit is set, then
the status line is unavailable).  The availability of window 1 is
flagged in bit 5: If that bit is set, then window 1 is
available. Window 0 is always available.

1.4.4 A game can select which window to use with the SET_WINDOW
op-code.  When window 1 is selected, its cursor position is reset to
the upper left-hand corner of that window.  Window 0 remembers its
cursor position.  Windows should not be cleared when selected.

1.4.5 Windows may be re-sized with the SPLIT_WINDOW op-code.  This
op-code rearranges the tiling of the screen into windows: The status
line (if available) is not affected by this: it is (if available)
always on line 1.  The position of window 1 remains un-changed (it
always begins at the line under the status line), but its height is
set to the number supplied with the op-code.  The position of window 0
is set to the line under window 1, and its height is adjusted to fill
the rest of the screen.  If window 1 grew by this process, its
additional lines should be cleared.  The appearance of window 0 should
not change, except for lines ``swallowed'' by window 1.  If by
re-sizing a window, the cursor would afterwards be outside the
selected one, it is re-set to the last position just inside it.  A
window may be of height 0, in which case ``the last position just
inside it'' is the end of the line which is its Y position coordinate.

1.4.6 The status line is automatically updated by the interpreter. It
can also be explicitly re-drawn by the game, by using the STATUS_LINE
op-code.  Nothing else can be done to the status line.

1.5 Clearing regions of the screen.

1.5.1 (See 1.4.5 on which lines should be cleared by the SPLIT_WINDOW
op-code.)  The screen should be cleared when the game starts or
re-starts.

1.6 Initial states.

1.6.1 When the game starts or re-starts, the upper window has height
0, the lower window is selected, and the cursor of the lower window is
on the left of its bottom line.

1.6.2 After a RESTORE, the upper window has height 0, and the lower
window is selected.


2 The Enhanced (EZIP, V4) screen model.

2.1 Text fonts and styles.

2.1.1 There is 1 font.

2.1.2 There are five text styles: ROMAN (numbered 0), BOLD (1), ITALIC
(2), REVERSE (4), and FIXED (8).  The font may be variable-pitched
(except when FIXED is selected), and combinations may be available.

2.1.3 Bit 2 of FLAGS-1 signals the availability of BOLD, bit 3:
Italic, and bit 4: FIXED.

2.1.4 Text display in windows 0 and 1 can be in any of the (available)
styles, except that when window 1 (upper) is selected, the FIXED
attribute is always set and cannot be turned off.  Style is selected
with the SET_TEXT_STYLE op-code.  Successive calls with non-roman
styles, or a bitmap of several styles binary-ORed together, may result
in combinations (such as bold italic).  Selecting ROMAN should turn
all styles off, except in the upper window, where the FIXED style
cannot be turned off. (???  Should bit 1 of FLAGS-2 be interpreted?
Should text attributes be connected to a window, or be global? global!)

2.2 Buffering.

2.2.1 Buffering is initially on (in both windows, since this is a
global property). Both windows share the same buffer.

2.2.2 Buffering can be controlled by the BUFFER_MODE op-code.  It
affects both windows.

2.3 Scrolling.

2.3.1 Scrolling is always off in window 1 (upper), and always on in
window 0 (lower).  Text that does not fit into the upper window is
simply not printed.  The cursor stays at the lower right-hand corner
in this case.  In the lower window, the customany [MORE] prompt is
printed.

2.4 Windowing.

2.4.1 There are two windows, numbered 0 and 1.

2.4.2 The windows are ordered in this way, from top to bottom: window
1, then window 0.

2.4.3 Both windows are always available.  There are no header flags.

2.4.4 A game can select which window to use with the SET_WINDOW
op-code.  When window 1 is selected, its cursor position is reset to
the upper left-hand corner of that window.  Window 0 remembers its
cursor position.  Windows are not cleared when selected.

2.4.5 Windows may be re-sized with the SPLIT_WINDOW op-code.  This
op-code rearranges the tiling of the screen into windows: The position
of window 1 remains un-changed (it is always at line 1), but its
height is set to the number supplied with the op-code.  The position
of window 0 is set to the line under window 1, and its height is
adjusted to fill the rest of the screen.  If window 1 grew by this
process, nothing should be cleared.  The appearance of the screen
should not change.  If by re-sizing a window, the cursor would
afterwards be outside the selected one, it is re-set to the last
position just inside it.  A window may be of height 0, in which case
``the last position just inside it'' is the end of the line which is
its Y position coordinate.

2.5 Clearing regions of the screen.

2.5.1 The op-code ERASE_WINDOW will erase one or both windows, and
perhaps re-arrange the tiling: Erasing window 0 will set its cursor in
the lower left-hand corner.  Erasing window 1 will set its cursor in
the upper left-hand corner.  Erasing window -1 will clear the whole
screen, collapse the upper window, and select the lower window,
setting its cursor in the lower left-hand corner.  Erasing window -2
will erase both windows, re-setting their respective cursors to the
upper left (window 1) and lower left (window 0) corners.  Except for
the case of window -1, the current window selection is not changed.

2.5.2 The op-code ERASE_LINE (Which only works in the upper window)
will erase the current line from the current cursor position onward.

2.6 Initial states.

2.6.1 When the game starts or re-starts, an implicit "ERASE_WINDOW -1"
is performed, and buffering is turned on.

2.6.2 After a RESTORE, the upper window is collapsed, and the lower
window is selected, as if by "SPLIT_WINDOW 0; SET_WINDOW 0"

2.7 cursor movement.

2.7.1 When the upper window is selected, the op-code SET_CURSOR will
set the cursor in the upper window to the desired line and column
(recall that in the upper window, the text style is always FIXED). No
effect if the lower window is selected.

2.7.2 When the upper window is selected, the op-code GET_CURSOR will
return the current cursor position. (??? Should an error condition
occur if the lower window is selected?)


3 The Advanced (XZIP, V5, V7, V8) screen model.

3.1 Text fonts and styles.

3.1.1 There are 4 fonts, numbered 1 to 4: a default font (1), a
picture font (2), a runic- and line-drawing-character font (3), and a
courier font. Font 2 is unspecified and need not be supplied.

3.1.2 There are five text styles: ROMAN (numbered 0), BOLD (1), ITALIC
(2), REVERSE (4), and FIXED (8).  The font may be variable-pitched
(except when FIXED is selected), and combinations may be available.

3.1.3 Bit 2 of FLAGS-1 signals the availability of BOLD, bit 3:
Italic, and bit 4: FIXED.  Availability of fonts is not documented in
the header, but by the return value of the SET_FONT op-code.

3.1.4 Text display in windows 0 and 1 can be in any of the (available)
styles and fonts, except that when window 1 (upper) is selected, the
FIXED attribute is always set and cannot be turned off.  Style is
selected with the SET_TEXT_STYLE op-code.  Successive calls with
non-roman styles, or a bitmap of several styles binary-ORed together,
may result in combinations (such as bold italic).  Selecting ROMAN
should turn all styles off, except in the upper window, where the
FIXED style cannot be turned off. (???  Should bit 1 of FLAGS-2 be
interpreted?  Should text attributes be connected to a window, or be
global?)

3.2 Buffering.

3.2.1 Buffering is initially on.  Output to the upper window, however,
is never buffered.

3.2.2 Buffering can be controlled by the BUFFER_MODE op-code.  It
affects only the lower window.

3.3 Scrolling.

3.3.1 Scrolling is always off in window 1 (upper), and always on in
window 0 (lower).  Text that does not fit into the upper window is
simply not printed.  The cursor stays at the lower right-hand corner
in this case.  In the lower window, the customany [MORE] prompt is
printed.

3.4 Windowing.

3.4.1 There are two windows, numbered 0 and 1.

3.4.2 The windows are ordered in this way, from top to bottom: window
1, then window 0.

3.4.3 Both windows are always available.  There are no header flags.

3.4.4 A game can select which window to use with the SET_WINDOW
op-code.  When window 1 is selected, its cursor position is reset to
the upper left-hand corner of that window.  Window 0 remembers its
cursor position. Windows are not cleared when selected.

3.4.5 Windows may be re-sized with the SPLIT_WINDOW op-code.  This
op-code rearranges the tiling of the screen into windows: The position
of window 1 remains un-changed (it is always at line 1), but its
height is set to the number supplied with the op-code.  The position
of window 0 is set to the line under window 1, and its height is
adjusted to fill the rest of the screen.  If window 1 grew by this
process, nothing should be cleared.  The appearance of the screen
should not change.  If by re-sizing a window, the cursor would
afterwards be outside the selected one, it is re-set in the following
way: in window 1, to the last position just inside it, in window 0, to
the first position just inside it.  A window may be of height 0, in
which case ``the last position just inside it'' is the end of the line
which is its Y position coordinate, and the ``first position'' is the
beginning of that line.

3.5 Clearing regions of the screen.

3.5.1 The op-code ERASE_WINDOW will erase one or both windows, and
perhaps re-arrange the tiling: Erasing window 0 will set its cursor in
the upper left-hand corner.  Erasing window 1 will set its cursor in
the upper left-hand corner.  Erasing window -1 will clear the whole
screen, collapse the upper window, and select the lower window,
setting its cursor in the upper left-hand corner.  Erasing window -2
will erase both windows, re-setting their respective cursors to the
upper left (window 1) and lower left (window 0) corners.  Except for
the case of window -1, the current window selection is not changed.

3.5.2 The op-code ERASE_LINE (Which only works in the upper window)
will erase the current line from the current cursor position onward.

3.6 Initial states.

3.6.1 When the game starts or re-starts, an implicit "ERASE_WINDOW -1"
is performed, and buffering is turned on.

3.6.2 After a successful RESTORE or RESTORE_UNDO, the upper window is
collapsed, and the lower window is selected, as if by "SPLIT_WINDOW 0;
SET_WINDOW 0"

3.7 Cursor movement.

3.7.1 When the upper window is selected, the op-code SET_CURSOR will
set the cursor in the upper window to the desired line and column
(recall that in the upper window, the text style is always FIXED). No
effect if the lower window is selected.

3.7.2 When the upper window is selected, the op-code GET_CURSOR will
return the current cursor position. (??? Should an error condition
occur if the lower window is selected?)

3.8 Text colour.

3.8.1 The foreground and background color of the text can be set with
the SET_COLOUR op-code. (globally).


4 The YZIP (Graphical, V6) screen model.

(I'll get to this later on).

