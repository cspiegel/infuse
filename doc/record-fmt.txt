record-fmt.txt - definition of the Infuse command record file format

Infuse record files, which are generated when games select output
stream 4, contain a transcript of commands entered by the user.

If these files only had to cater to V3, they would be simple,
containing one command per line. Later versions add complications like
single-character input, timeouts, and mouse clicks, and these items
should be incorporated into the format.

The definition of a record file runs thus (in EBNF):

	<record file> = { { <input character> } <EOL> } <EOF> .

<input character>s are the characters permissible by the standard, 
as well as the notation [...] for special keys

	[[]	[
	[BS]	rubout
	[CR]	enter
	[ESC]	escape
	[UP]
	[LEFT]
	[RIGHT]
	[DOWN]	cursor keys
	[F1]
	...
	[F12]	function keys
	[KP0]
	...
	[KP9]	keypad keys
	[MENU|x,y,b,m,e]
	[CLICK|x,y,b,m,e]
	[DCLICK|x,y,b,m,e]
		menu and single/double mouse click, with mouse table entries
	[TIME]	timeout
	[FILE|filename]
		file name spec.

An example file (for BZORK)

begin[CR]
[CR]
[DOWN]
[CR]
[DOWN]
[DOWN]
[CR]
yes[CR]
[CR]
yotznick[CR]
y[CR]
sure[CR]
nope[CR]
[CR]
[KP7]
read bill[CR]
read bilboard.pick a weed.[KP3]
[KP6]
[KP6]
hello, sailor[CR]
take it[CR]
wield it[CR]
[KP4]
[KP2]
take lamp.open door[KP4]
open door.[KP4]
[KP4]
take it.[CR]
[KP4]
cook,what about onion[CR]
open door[CR]
d[CR]
d[CR]
take scroll[CR]
read it[CR]
[F7]
[F7]
[F4]
the shillelagh[CR]
quit[CR]
yes[CR]
[CR]

This format violates the standard 0.2, but it is more flexible.
