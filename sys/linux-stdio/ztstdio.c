/* ztstdio.c - LINUX/stdio z-term
   
   This file is part of Infuse
   
   INFUSE COPYRIGHT
   ================
   Infuse is a generic z-machine emulator.
   
   Copyright (C) 1997, 2019 by Florian M. Weps.
   
      Florian M. Weps
      Dieffenbachstr. 26
      10967 Berlin
      Germany
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of Version 3 of the GNU General Public License as
   published by the Free Software Foundation.
   
   This program is distributed WITHOUT ANY WARRANTY; without even the
   implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
   PURPOSE.  See Version 3 of the GNU General Public License for more
   details.
   
   You should have received a copy of Version 3 of the GNU General Public
   License along with this program. If not, you can FTP the license from
   prep.ai.mit.edu/pub/gnu/COPYING-3 or write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#define _XOPEN_SOURCE_EXTENDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strerror() */
#include <wchar.h>
#include <locale.h>
#include <termios.h>
#include <unistd.h>
#include "zterm.h"

char *zbanne = "Linux/stdio"; /* machine-dependent part of banner */
int ztever = 1;  /* version number of this z-term */
int zw_inu = 0;  /* terminal width in units */
int zh_inu = 0;  /* terminal height in units */
int zmwinu = 1;  /* monospace font width in units */
int zfhinu = 1;  /* font height in units */
int zhassn = 0;  /* flag: sound effects possible */
int zhaspi = 0;  /* flag: pix display possible */
int zhasco = 0;  /* flag: colours available */
int zhasmo = 0;  /* flag: mouse available */
int zhasmn = 0;  /* flag: menus available */
int zhastm = 0;  /* flag: timed input available */
int zf1pro = 0;  /* flag: font 1 is proportionally spaced */
int zhasf2 = 0;  /* flag: font 2 available */
int zhasf3 = 0;  /* flag: font 3 available */
int zhasf4 = 0;  /* flag: font 4 available */
int zstyle = (Z_ROMAN | Z_FIXED);

static int prev_y = -1;

void (*zucmou) (int x, int y, int b, int m, int e); /* mouse up-call */

void
z_init (int *pargc, char *argv[])
{
  setlocale(LC_CTYPE, "");
  zw_inu = 80;
  zh_inu = 255; // std 1.1 - "infinite height, never pause to print [more]"
}

void
z_exit (void)
{
  putchar ('\n');
}

void
zsescr (int top, int bot)
{
  /* not used in core 1.57 */
}

/* zscrol - scroll up n units in scroll region */

void
zscrol (int n)
{
  /* not used in core 1.57 */
}

void
zcpbox (int x, int y, int w, int h, int x1, int y1)
{
  -- prev_y;
}

void
zshbox (int x, int y, int w, int h, int c)
{
}

void
zshpix (int x, int y, int w, int h, void *img)
{
  /* not supported in text mode */
}

void
zshcur (int x, int y, int fg, int bg)
{
}

void
zhdcur (void)
{
  /* can't do that / not necessary in text mode */
}

/* zshstr - draw string at position. The alignment parameter align is
   not used in core 1.57 */

void
zshstr (int x, int y, int w, int align, z_glyph *s, int n)
{
  int i;
  if (y != prev_y) putwchar (L'\n');
  prev_y = y;
  for (i = 0; i < n; i++) {
    putwchar ((wchar_t)s[i].ch);
    //fprintf (stderr, "%x\t%lc\n", (int)s[i].ch, s[i].ch);
  }
}

int
zstrwd (z_glyph *s, int n)
{
  return n;
}

static wchar_t
input_wchar(void)
{
    struct termios oldt,newt;
    wchar_t ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getwchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return ch;
}

int
zrdchr (long *wait_ms)
{
  wchar_t c;
  int i;
  unsigned char b;
  c = input_wchar ();
  b = 0x20; /* default value */
  switch (c)
    {
    case 27: /* ESC */
      b = Z_ESC;
      break;
    case 8:
    case 127:
      b = Z_BS; /* signal BS - only key not in table of �10.7 */
      break;
    case 10:
    case 13:
      b = Z_CR; /* LF is erroneously recommended in �10.7 */
      break;
    default:
      b = 0x20;
      if (c > 0x20 && c < 0x7F) {
	/* printable ascii */
	b = c;
      } else if (c > 0xA0) {
	/* printable UNICODE */
	for (i = 155; i <= 251; i++) {
	  if (cunitt[i - 155] == c) {
	    /* search UNICODE code in x-lation table */
	    b = i;
	    break;
	  }
	}
      }
      break;
    }
  return b;
}
  
void
zflush (void)
{
}

