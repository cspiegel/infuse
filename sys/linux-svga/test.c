#include <vga.h>
#include <vgagl.h>
#include <stdlib.h>
#include "gem_text.h"

int VGAMODE;
int VIRTUAL;
GraphicsContext *backscreen;
GraphicsContext *physicalscreen;
void *expanded_font;

int main (int argc, char **argv)
{
  vga_init ();
  VGAMODE = vga_getdefaultmode();
  if (VGAMODE == -1)
    VGAMODE = G640x480x256;
  if (!vga_hasmode (VGAMODE))
    {
      printf ("Mode not available.\n");
      exit (EXIT_FAILURE);
    }
  VIRTUAL = 0;
  if (vga_getmodeinfo (VGAMODE)->colors == 16 ||
      (vga_getmodeinfo (VGAMODE)->flags & IS_MODEX))
    VIRTUAL = 1;
  if (VIRTUAL)
    {
      gl_setcontextvgavirtual (VGAMODE);
      backscreen = gl_allocatecontext ();
      gl_getcontext (backscreen);
    }
  vga_setmode (VGAMODE);
  gl_setcontextvga (VGAMODE);
  physicalscreen = gl_allocatecontext ();
  gl_getcontext (physicalscreen);
  if (COLORS == 256)
    gl_setrgbpalette ();
  if (VIRTUAL)
    gl_setcontext (backscreen);

  /* gl text */

  expanded_font = malloc (256L * 8L * 8L * BYTESPERPIXEL);
  gl_expandfont (8, 8, 15, gl_font8x8, expanded_font);
  gl_setfont (8, 8, expanded_font);
  gl_setwritemode (FONT_EXPANDED|WRITEMODE_MASKED);

  /* gem text */

  gem_init ();

  /*--- begin test ---*/
  gl_clearscreen (0);
  gl_line (10, 10, 50, 10, 1);
  gl_line (50, 10, 50, 50, 2);
  gl_line (50, 50, 10, 50, 3);
  gl_line (10, 50, 10, 10, 4);
  gl_write (10, 50, "Hello, World!");
  gem_set_font (GEM_FONT_1, GEM_FONT_TALL, 15, 0, GEM_FONT_ROMAN);
  gem_text (10, 70, "Hello, World!");
  gem_set_font (GEM_FONT_1, GEM_FONT_TALL, 15, 0, GEM_FONT_REVERSE);
  gem_text (10, 100, "Hello, World!");
  gem_set_font (GEM_FONT_1, GEM_FONT_TALL, 15, 0, GEM_FONT_ROMAN);
  gem_justified_text (10, 120, gem_get_string_w ("Hello, World!") + 40,
		      GEM_LEFT_JUSTIFIED, "Hello, World!");
  gem_justified_text (10, 140, gem_get_string_w ("Hello, World!") + 40,
		      GEM_RIGHT_JUSTIFIED, "Hello, World!");
  gem_justified_text (10, 160, gem_get_string_w ("Hello, World!") + 40,
		      GEM_CENTER_JUSTIFIED, "Hello, World!");
  gem_justified_text (10, 180, gem_get_string_w ("Hello, World!") + 40,
		      GEM_FULL_JUSTIFIED, "Hello, World!");
  gem_justified_text (10, 200, gem_get_string_w ("Hello, World!") - 20,
		      GEM_FULL_JUSTIFIED, "Hello, World!");
  gl_fillbox (10, 50, 8, 8, 2);
  if (VIRTUAL)
    gl_copyscreen (physicalscreen);
  getchar ();
  /*--- end test ---*/

  if (VIRTUAL)
    gl_freecontext (backscreen);
  vga_setmode (TEXT);
  return EXIT_SUCCESS;
}
