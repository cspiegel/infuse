/* svga.c - LINUX-SVGALIB console I/O driver for Infuse.
 * This file is part of Infuse.
 * Copyright (C) 1997, 1998 by Florian M. Weps.  All rights reserved.
 */

#define USE_RAW_KEYBOARD 0 /* not implemented */

#include <curses.h> /* for reading the keyboard */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vga.h>
#include <vgagl.h>

#include "console.h"
#include "gem_text.h"

#define DEBUG 0

#if DEBUG
static FILE *myerr;
#endif

#if 0
# define VGAMODE G320x200x256
#else
# define VGAMODE G640x480x256
#endif

static int VIRTUAL;
static GraphicsContext *backscreen;
static GraphicsContext *physicalscreen;

static char *banner = "Linux/svgalib";
static int version  = 1;     /* added to the letter '0' */
static int font_size;        /* GEM_TALL or GEM_SMALL */
static int mono_w;           /* width of a '0' in monospaced font */
static int line_h;           /* height in pixels of fonts */
static int scroll_top = 0;   /* first scrolling line */
static int scroll_bot = 0;   /* first non-scrolling line */
static int cursor_x = 0;     /* cursor position */
static int cursor_y = 0;
static int cursor_fg_color;
static int cursor_bg_color;

static int keyboard_fd;      /* file descriptor of keyboard; for select() */

/* z_palette - colors 0 and 1 are not really colors, and need not
 * interest us here. */

typedef enum {
  CGA_BLACK, CGA_BLUE, CGA_GREEN, CGA_CYAN, CGA_RED, CGA_MAGENTA, CGA_BROWN,
  CGA_LGRAY, 
  CGA_DGRAY, CGA_LBLUE, CGA_LGREEN, CGA_LCYAN, CGA_LRED, CGA_LMAGENTA,
  CGA_YELLOW, CGA_WHITE
} CGA_color;

static int z_palette[10] = {0, 0,
			    CGA_BLACK, CGA_RED, CGA_GREEN, CGA_YELLOW,
			    CGA_BLUE, CGA_MAGENTA, CGA_CYAN, CGA_WHITE};

#if USE_RAW_KEYBOARD
#else
static int z_to_iso1[256] = {
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 
  0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 
  0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 
  0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 
  0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 
  0x3C, 0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 
  0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 
  0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F, 
  0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 
  0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 
  0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xE4, 
  0xF6, 0xFC, 0xC4, 0xD6, 0xDC, 0xDF, 0xAB, 0xBB, 0xEB, 0xEF, 0xFF, 0xCB, 
  0xCF, 0xE1, 0xE9, 0xED, 0xF3, 0xFA, 0xFD, 0xC1, 0xC9, 0xCD, 0xD3, 0xDA, 
  0xDD, 0xE0, 0xE8, 0xEC, 0xF2, 0xF9, 0xC0, 0xC8, 0xCC, 0xD2, 0xD9, 0xE2, 
  0xEA, 0xEE, 0xF4, 0xFB, 0xC2, 0xCA, 0xCE, 0xD4, 0xDB, 0xE5, 0xC5, 0xF8, 
  0xD8, 0xE3, 0xF1, 0xF5, 0xC3, 0xD1, 0xD5, 0xE6, 0xC6, 0xE7, 0xC7, 0xFE, 
  0xF0, 0xDE, 0xD0, 0xA3, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20
};
#endif

/* c_init - here, we switch to the selected graphics mode and fill in
 * the c_info structure. */

c_info
c_init (void)
{
  c_info ret;
#if DEBUG
  myerr = fopen ("svga.err", "w");
#endif
  /* set up vga mode and gl */
  vga_init ();
  if (!vga_hasmode (VGAMODE))
    {
      fprintf (stderr, "c_init: vga mode %d not available\n", VGAMODE);
      c_exit (); /* cleanup routine not yet initialized */
      exit (EXIT_FAILURE);      
    }
#if USE_RAW_KEYBOARD
#else
  initscr ();
  cbreak ();
  noecho ();
  nonl ();
  intrflush (stdscr, FALSE);
  keypad (stdscr, TRUE);
#endif
  if (vga_getmodeinfo (VGAMODE)->colors == 16 ||
      (vga_getmodeinfo (VGAMODE)->flags & IS_MODEX))
    VIRTUAL = 1;
  else
    VIRTUAL = 0;
  if (VIRTUAL)
    {
      gl_setcontextvgavirtual (VGAMODE);
      backscreen = gl_allocatecontext ();
      gl_getcontext (backscreen);
    }
  vga_setmode (VGAMODE);
  gl_setcontextvga (VGAMODE);
  physicalscreen = gl_allocatecontext ();
  gl_getcontext (physicalscreen);
  /* set up the text output module */
  gem_init ();
  gl_setclippingwindow (0, 0, WIDTH-1, HEIGHT-1);
  gl_enableclipping ();
#if 1
  font_size = HEIGHT > 200 ? GEM_FONT_TALL : GEM_FONT_SMALL;
#else
  font_size = GEM_FONT_SMALL;
#endif
  gem_set_font (GEM_FONT_4, font_size,
		z_palette[C_CYAN], z_palette[C_BLUE], GEM_FONT_ROMAN);
  mono_w = gem_get_char_w ('0');
  line_h = gem_get_font_h ();
  /* fill in the info structure */
  ret = malloc (sizeof (*ret));
  if (!ret)
    {
      fprintf (stderr, "c_init: out of dynamic memory\n");
      c_exit (); /* since the cleanup routine was not installed yet */
      exit (EXIT_FAILURE);
    }
  ret->banner = banner;
  ret->version = '0' + version;
  ret->screen_w_in_units = WIDTH;
  ret->screen_h_in_units = HEIGHT;
  ret->font_w_in_units = mono_w;
  ret->font_h_in_units = line_h;
  ret->has_sound = 0; /* beeps are annoying */
  ret->has_pix = 0;
  ret->has_colors = 1;
  ret->has_mouse = 0;
  ret->has_timer = 1;
  ret->has_menus = 0;
  ret->font1_is_prop = 1;
  ret->has_font2 = 0;
  ret->has_font3 = 1;
  ret->has_font4 = 1;
  ret->has_styles = (C_ROMAN|C_REVERSE|C_ITALIC|C_BOLD|C_FIXED);
  return ret;
}

/* c_exit - here, we clean up */

void
c_exit (void)
{
  vga_setmode (TEXT);
#if USE_RAW_KEYBOARD
#else
  endwin ();
#endif
}

/* c_sefnt - set the font, style, and colors to use for text output
 * and font metric information. */

int
csefnt (c_font f, c_style s, c_color fg, c_color bg)
{
  int id = GEM_FONT_1;        /* default font */
  int style = GEM_FONT_ROMAN; /* default style */
  if (f == C_FONT_1)
    id = GEM_FONT_1;
  else if (f == C_FONT_3)
    id = GEM_FONT_3;
  else if (f == C_FONT_4)
    id = GEM_FONT_4;
  if (s & C_REVERSE)
    style |= GEM_FONT_REVERSE;
  if (s & C_BOLD)
    style |= GEM_FONT_BOLD;
  if (s & C_ITALIC)
    style |= GEM_FONT_ITALIC;
  if (s & C_FIXED)
    id = f == C_FONT_1 ? GEM_FONT_4 : id; /* switch only if prop */
  cursor_fg_color = z_palette[fg];
  cursor_bg_color = z_palette[bg];
#if 0
  fprintf (myerr, "gem_set_font: %d, %d, %d, %d, %d\n",
	   id, font_size, z_palette[fg], z_palette[bg], style);
#endif
  gem_set_font (id, font_size, z_palette[fg], z_palette[bg], style);
}

/* csescr - set scrolling region. top is the first line which scrolls,
 * and bot is first one which doesn't. */

void
csescr (int top, int bot)
{
  scroll_top = top - 1;
  scroll_bot = bot - 1;
#if DEBUG
  fprintf (myerr, "scroll_top: %d\nscroll_bot: %d\n", scroll_top, scroll_bot);
#endif
}

/* csecur - set cursor position. This just sets the global variables;
 * you have to turn the cursor off yourself. */

void
csecur (int x, int y)
{
  cursor_x = x - 1;
  cursor_y = y - 1;
#if DEBUG
  fprintf (myerr, "cursor_x: %d\ncursor_y: %d\n", cursor_x, cursor_y);
#endif
}

/* csemow - set the mouse window. Mouse clicks should only be reported
 * when the pointer is in this window, and if possible the pointer
 * should not be allowed to move outside it. */

void
csemow (int x, int y, int w, int h)
{
#if DEBUG
  fprintf (dfp, "csemou (%d, %d, %d, %d)\n", x, y, w, h);
#endif
}

/* cscrol - scroll up n lines in the scroll region. You have to set
 * scroll_top and scroll_bot first. Also, fill in the exposed bottom
 * lines with the desired color. */

void
cscrol (int n, int c)
{
#if DEBUG
  fprintf (myerr, "copybox: %d, %d, %d, %d; %d, %d\n",
	   0, scroll_top + n, WIDTH, scroll_bot - scroll_top - n,
	   0, scroll_top);
#endif
  gl_copybox (0, scroll_top + n, WIDTH, scroll_bot - scroll_top - n,
	      0, scroll_top);
#if DEBUG
  fprintf (myerr, "fillbox: %d, %d, %d, %d; %d\n",
	   0, scroll_bot - n, WIDTH, n, z_palette[c]);
#endif
  gl_fillbox (0, scroll_bot - n, WIDTH, n, z_palette[c]);
}

/* ccpbox - copy rectangular area on screen. (generalized scroll) */

void
ccpbox (int x, int y, int w, int h, int x1, int y1)
{
#if DEBUG
  fprintf (myerr, "copybox: %d, %d, %d, %d; %d, %d\n",
	   x-1, y-1, w, h, x1-1, y1-1);
#endif
  gl_copybox (x-1, y-1, w, h, x1-1, y1-1);
}

/* cshbox - fill rectangular area with color */

void
cshbox (int x, int y, int w, int h, c_color c)
{
#if DEBUG
  fprintf (myerr, "fillbox: %d, %d, %d, %d; %d\n",
	   x-1, y-1, w, h, z_palette[c]);
#endif
  gl_fillbox (x-1, y-1, w, h, z_palette[c]);
}

/* cshchr - print a character in the selected font, style and color */

void
cshchr (int x, int y, unsigned char c)
{
  unsigned char t[2];
  t[0] = c;
  t[1] = '\0';
#if DEBUG
  fprintf (myerr, "gem_text: %d, %d, %s\n", x-1, y-1, t);
#endif
  gem_text (x-1, y-1, t);
}

/* cshstr - print a justified string in the selected text style */

void
cshstr (int x, int y, int w, c_align a, unsigned char *s)
{
  int alignment = GEM_LEFT_JUSTIFIED;
  if (a == C_LEFT_ALIGN)
    alignment = GEM_LEFT_JUSTIFIED;
  else if (a == C_RIGHT_ALIGN)
    alignment = GEM_RIGHT_JUSTIFIED;
  else if (a == C_CENTER_ALIGN)
    alignment = GEM_CENTER_JUSTIFIED;
  else if (a == C_FULL_ALIGN)
    alignment = GEM_FULL_JUSTIFIED;
#if DEBUG
  fprintf (myerr, "gem_justified_text: %d, %d, %d, %d, %s\n", 
	   x-1, y-1, w, alignment, s);
#endif
  gem_justified_text (x-1, y-1, w, alignment, s);
}

/* cshpix - display a picture. Since this driver does not support
 * pictures, this is a no-op. */

void
cshpix (int x, int y, int n)
{
}

/* cshcur - turn the cursor on or off. The cursor is a vertical line
 * of width 1, which extends downward from the current cursor
 * position. The length of the cursor is the height of the font. */

void
cshcur (c_crsr state)
{
#if DEBUG
  fprintf (myerr, "line: %d, %d, %d, %d; %d\n", 
	   cursor_x, cursor_y, cursor_x, cursor_y + line_h - 1,
	   state == C_ON ? cursor_fg_color : cursor_bg_color);
#endif
  gl_line (cursor_x, cursor_y, cursor_x, cursor_y + line_h - 1,
	   state == C_ON ? cursor_fg_color : cursor_bg_color);
}

/* cshmnu - display a menu. Since this deiver does not support menus,
 * this is a no-op. */

void
cshmnu (int id, unsigned char *menu[])
{
}

/* cflush - flush any buffered output. A no-op in this driver, since
 * it does everything in real-time - except that if a 16-color mode is
 * used with gl, then we copy the virtual screen to the real
 * screen. */

void
cflush (void)
{
  if (VIRTUAL)
    gl_copyscreen (physicalscreen);
}

/* csound - play a sound effect. Since this driver does not support
 * sound effects, this is a no-op. */

void
csound (int number, c_soundfx e, int volume, int *finished)
{
}

/* cpixwh - return picture metrics. Since this diver does not support
 * pictures, this is a no-op. */

int
cpixwh (int n, int *w, int *h)
{
  *w = 0;
  *h = 0;
  return 0;
}

/* cchrwd - return character size */

int
cchrwd (int c)
{
  return gem_get_char_w (c);
}

/* cstrwd - return string width */

int
cstrwd (unsigned char *s)
{
#if 0
  int w = 0;
  while (*s)
    w += cchrwd (*s++);
  return w;
#endif
  return gem_get_string_w (s);
}

/* crdchr - keyboard / timer / mouse input - if the timer expires, 0
 * is returned by crdchr().  If a mouse click occurred, one of the
 * mouse click codes are returned by crdchr().  In any case, the mouse
 * coordinates, mouse button states, menu selection, and the color of
 * the pixel under the mouse cursor can always be requested by the
 * crdmou() function. Since this driver does not support timers, we
 * simply wait for a key to be pressed.  If a negative argument is
 * passed for the time to wait, we wait until a key is pressed. */

unsigned char
crdchr (int ms_to_wait)
{
  int c, i;
  unsigned char b;
#if DEBUG
  fprintf (dfp, "crdchr (%d)\n", ms_to_wait);
#endif
  ms_to_wait /= 100;
  if (ms_to_wait > 0)
    {
      do
	{
	  halfdelay (ms_to_wait > 255 ? 255 : ms_to_wait);
	  c = getch ();
	  nocbreak (); /* leave half-delay mode */
	  cbreak ();
	  ms_to_wait -= 255;
	}
      while (c == ERR && ms_to_wait > 0);
    }
  else
    c = getch ();
  b = 0x20; /* default value */
  switch (c)
    {
    case ERR: /* timeout */
      b = 0;
      break;
    case 27: /* ESC */
      b = 27;
      break;
    case KEY_BACKSPACE:
    case KEY_DC: /* delete character */
    case 8:
    case 127:
      b = 0x08; /* signal BS - only key not in table of �10.7 */
      break;
    case 10:
    case 13:
    case KEY_ENTER:
      b = 0x0A; /* LF - recommendation of �10.7 */
      break;
    case KEY_DOWN:
      b = 130;
      break;
    case KEY_UP:
      b = 129;
      break;
    case KEY_LEFT:
      b = 131;
      break;
    case KEY_RIGHT:
      b = 132;
      break;
    case KEY_F(1):
    case KEY_F(2):
    case KEY_F(3):
    case KEY_F(4):
    case KEY_F(5):
    case KEY_F(6):
    case KEY_F(7):
    case KEY_F(8):
    case KEY_F(9):
    case KEY_F(10):
    case KEY_F(11):
    case KEY_F(12):
      b = (c - KEY_F(1)) + 133;
      break;
    case KEY_IC: /* keypad 0 */
      b = 145;
      break;
    case KEY_C1: /* keypad 1 */
      b = 146;
      break;
    case KEY_C3: /* keypad 3 */
      b = 148;
      break;
    case KEY_B2: /* keypad 5 */
      b = 150;
      break;
    case KEY_A1: /* keypad 7 */
      b = 152;
      break;
    case KEY_A3: /* keypad 9 */
      b = 154;
      break;
    default:
      b = 0x20;
      if (c > 0x20 && c < 0x7F) /* printable ascii */
	b = c;
      else if (c > 0xA0 && c <= 0xFF) /* printable ISO-1 */
	for (i = 0; i < 256; i++)
	  {
	    if (z_to_iso1[i] == c) /* search iso code in x-lation table */
	      {
		b = i;
		break;
	      }
	  }
      break;
    }
  return b;
}

/* crdmou - return the current mouse state. Since this driver does not
 * handle the mouse, this is a no-op. */

void
crdmou (int *x, int *y, int *buttons, int *menu, c_color *at_xy)
{
}

/* hooks are all empty */

char (*cgetop) (int, char**) = NULL;
int (*cfsele) (int, unsigned char*) = NULL;
unsigned char (*caread) (unsigned char*, int, int, int (*)(void)) = NULL;
void (*cscrip) (c_font, c_style, c_color, c_color, unsigned char) = NULL;
int  (*cscrim) (c_font, c_style, c_color, c_color, unsigned char) = NULL;
void (*cpixtb) (int, int*) = NULL;

/* log - RCS
 * $Log: svga.c,v $
 * Revision 1.1.1.1  2004/07/21 14:43:21  fmw
 * Imported sources
 *
 * Revision 1.7  1998/01/12 17:01:05  fmw
 * some buggy input codes fixed
 *
 * Revision 1.6  1998/01/08 16:30:20  fmw
 * forgot to set the timer flag in the init procedure
 *
 * Revision 1.5  1998/01/08 16:27:29  fmw
 * added timed keyboard input (ncursrs)
 *
 * Revision 1.4  1998/01/08 08:49:49  fmw
 * added DEBUG constant
 *
 * Revision 1.3  1997/12/30 17:08:15  fmw
 * in-module ncurses keyboard support
 *
 * Revision 1.2  1997/12/30 09:46:31  fmw
 * minimal driver with everything but keyboard support.
 *
 * Revision 1.1  1997/12/24 08:34:53  fmw
 * Initial revision
 *
 */
