/* gem_text - replacements for svgalib/gl text routines, for GEM fonts.
 * This file is part of INFUSE.
 * Copyright (C) 1997, 1998 by Florian M. Weps.  All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <vga.h>
#include <vgagl.h>
#include "gem_text.h" /* GEM font header structure definition */

/* GEM fonts. These are converted from binary GEM font files into C
 * defintions by the utility gem2c, and compiled by the C
 * compiler. Each such file exports a GEM font header structure. */

extern GEM_FONT gem_font_501_8;  /* small font 1 (proportional) */
extern GEM_FONT gem_font_501_16; /* tall  font 1 (proportional) */
extern GEM_FONT gem_font_503_8;  /* small font 3 (monospace) */
extern GEM_FONT gem_font_503_16; /* tall  font 3 (monospace) */
extern GEM_FONT gem_font_504_8;  /* small font 4 (monospace) */
extern GEM_FONT gem_font_504_16; /* tall  font 4 (monospace) */

/* the list of fonts - chained by gem_init() */

static GEM_FONT *font_list = NULL;

/* the current font and its pixmap */

static GEM_FONT *current_font = NULL;
static unsigned char *font_map = NULL; /* set up by gem_set_font */
static long font_map_size = 0; /* in bytes */
static int current_font_fg, current_font_bg;
static int current_style;

/* font_to_fontmap - inflate the monochrome bitmap of the font into a color
 * pixmap. Used by gem_set_font() */

static void
font_to_fontmap (GEM_FONT *font, int fg, int bg)
{
  unsigned char *p, *bit_map;
  int i, j, byte_width, lines;
  bit_map = font->font_address;
  byte_width = font->byte_width;
  lines = font->scan_lines;
  if (font_map_size < 8L * byte_width * lines * BYTESPERPIXEL)
    {
      font_map_size = 8L * byte_width * lines * BYTESPERPIXEL;
      font_map = malloc (font_map_size);
      if (!font_map)
	font_map_size = 0;
    }
  if (font_map)
    {
      p = font_map;
      for (i = 0; i < byte_width * lines; i++)
	{
	  for (j = 0; j < 8; j++)
	    {
	      if (*bit_map & (0x80 >> j)) /* bit set? */
		{
		  switch (BYTESPERPIXEL)
		    {
		    case 1 :
		      *p++ = fg;
		      break;
		    case 2 :
		      *(unsigned short *)p = fg;
		      p += 2;
		      break;
		    case 3 :
		      *(unsigned short *)p = fg;
		      *(p + 2) = fg >> 16;
		      p += 3;
		      break;
		    case 4 :
		      *(unsigned int *)p = fg;
		      p += 4;
		      break;
		    }
		}
	      else
		{
		  switch (BYTESPERPIXEL)
		    {
		    case 1 :
		      *p++ = bg;
		      break;
		    case 2 :
		      *(unsigned short *)p = bg;
		      p += 2;
		      break;
		    case 3 :
		      *(unsigned short *)p = bg;
		      *(p + 2) = bg >> 16;
		      p += 3;
		      break;
		    case 4 :
		      *(unsigned int *)p = bg;
		      p += 4;
		      break;
		    }
		}
	    }
	  bit_map++;
	}
    }
}

/* append_font - aux fun used by gem_init */

static GEM_FONT **
append_font (GEM_FONT **p, GEM_FONT *f)
{
  *p = f;
  return &f->next_font;
}

/* gem_init - set up the font list */

void
gem_init (void)
{
  GEM_FONT **p;
  p = &font_list;
  p = append_font (p, &gem_font_504_8);
  p = append_font (p, &gem_font_504_16);
  p = append_font (p, &gem_font_503_8);
  p = append_font (p, &gem_font_503_16);
  p = append_font (p, &gem_font_501_8);
  p = append_font (p, &gem_font_501_16);
  *p = NULL; /* just to be certain */
  if (font_map)
    free (font_map);
  current_font = NULL;
}

/* gem_set_font - set up the font to use for subsequent gem_text()
 * operations. The style is ignored for now, except for REVERSE. */

void
gem_set_font (int id, int size, int fg, int bg, int style)
{
  GEM_FONT *p;
  if (!font_list)
    {
      fprintf (stderr, "gem_set_font: font_list not initialized!\n");
      exit (EXIT_FAILURE);
    }
  p = font_list;
  while (p && !(p->id == id && p->scan_lines == size))
    p = p->next_font;
  if (!p)
    p = font_list; /* use the small font 4 by default */
  if (current_font && p == current_font && fg == current_font_fg
      && bg == current_font_bg && style == current_style)
    return;
  current_font = p;
  fg += style & GEM_FONT_BOLD ? 8 : 0;
  current_font_fg = fg;
  current_font_bg = bg;
  current_style = style;
  if (style & GEM_FONT_REVERSE)
    font_to_fontmap (current_font, bg, fg);
  else
    font_to_fontmap (current_font, fg, bg);
}

int
gem_text (int x, int y, unsigned char *s)
{
  int w, wsum = 0, bw, h, offset;
  if (!current_font)
    {
      fprintf (stderr, "gem_text: current_font not set!\n");
      exit (EXIT_FAILURE);
    }
  h = current_font->scan_lines;
  bw = current_font->byte_width * 8;
  while (*s)
    {
      if (*s >= current_font->first_ascii && *s <= current_font->last_ascii)
	{
	  offset = current_font->cot_address[*s - current_font->first_ascii];
	  w = current_font->cot_address[*s - current_font->first_ascii + 1]
	    - offset;
	}
      else
	{
	  offset = -1;
	  w = current_font->max_box_width;
	}
      wsum += w; /* must be here! */
      if (x + w <= WIDTH && y + h <= HEIGHT)
	{
	  if (offset >= 0)
	    gl_putboxpart (x, y, w, h, bw, h, font_map, offset, 0);
	  else /* char not available */
	    gl_fillbox (x, y, w, h, current_font_fg);
	}
      else
	break;
      x += w;
      s++;
    }
  return wsum;
}

/* gem_get_font_h - read-only export of font height */

int
gem_get_font_h (void)
{
  if (!current_font)
    {
      fprintf (stderr, "gem_get_font_h: current_font not set!\n");
      exit (EXIT_FAILURE);
    }
  return current_font->scan_lines;
}

/* gem_get_char_w - get character width */

int
gem_get_char_w (unsigned char c)
{  
  if (!current_font)
    {
      fprintf (stderr, "gem_get_char_w: current_font not set!\n");
      exit (EXIT_FAILURE);
    }
  if (c >= current_font->first_ascii && c <= current_font->last_ascii)
    return current_font->cot_address[c - current_font->first_ascii + 1]
      - current_font->cot_address[c - current_font->first_ascii];
  else
    return current_font->max_box_width;
}

/* gem_get_char_w - get width of an entire string. */

int
gem_get_string_w (unsigned char *s)
{
  int w = 0;
  while (*s)
    w += gem_get_char_w (*s++);
  return w;
}

/* gem_justified_text - print aligned text in a box. If the string is
 * too wide for the box, it is compressed into the box as if
 * justification were FULL, regardless of the real setting of the
 * justification. */

void
gem_justified_text (int x, int y, int w, int just, unsigned char *s)
{
  int inter_char_pad = 0; /* flags inter_character_padding */
  int blanks; /* number of blank spaces in s (for full justification) */
  int length; /* number of characters in s (for full justification) */
  int pad;    /* total amount of padding to insert */
  int pw;     /* amount of padding ot insert after current char */
  int cw;     /* width of current character */
  int offset; /* x-offset of current character into bitmap */
  int bw;     /* width in pixels of current font bitmap */
  int h;      /* height of current font */
  if (!current_font)
    {
      fprintf (stderr, "gem_text: current_font not set!\n");
      exit (EXIT_FAILURE);
    }
  h = current_font->scan_lines;
  bw = current_font->byte_width * 8;
#if 0
  gl_fillbox (x, y, w, h,
	      current_style & GEM_FONT_REVERSE ?
	      current_font_bg : current_font_fg);
#else
  gl_fillbox (x, y, w, h,
	      current_style & GEM_FONT_REVERSE ?
	      current_font_fg : current_font_bg);
#endif
  pad = w - gem_get_string_w (s);
  if (pad < 0)
    just = GEM_FULL_JUSTIFIED;
  if (just == GEM_RIGHT_JUSTIFIED)
    x += pad;
  else if (just == GEM_CENTER_JUSTIFIED)
    x += pad/2;
  else if (just == GEM_FULL_JUSTIFIED)
    {
      for (blanks = length = 0; s[length]; length++)
	if (s[length] == 0x20)
	  blanks++;
      if (!blanks || pad / blanks < 0
	  || pad / blanks > gem_get_char_w(32)/2)
	inter_char_pad = 1;
      length--; /* only pad *between* characters */
    }
  while (*s)
    {
      pw = 0;
      if (*s >= current_font->first_ascii && *s <= current_font->last_ascii)
	{
	  offset = current_font->cot_address[*s - current_font->first_ascii];
	  cw = current_font->cot_address[*s - current_font->first_ascii + 1]
	    - offset;
	}
      else
	{
	  offset = -1;
	  cw = current_font->max_box_width;
	}
      if (x + cw <= WIDTH && y + h <= HEIGHT)
	{
	  if (just == GEM_FULL_JUSTIFIED)
	    {
	      if (inter_char_pad && length)
		{
		  pw = pad / length;
		  pad -= pw;
		  length--;
		}
	      else if (*s == 0x20)
		{
		  pw = pad / blanks;
		  pad -= pw;
		  blanks--;
		}
	    }
	  if (offset >= 0)
	    {
	      if (current_style & GEM_FONT_ITALIC)
		{
		  int i;
		  for (i = 0; i < 4; i++)
		    gl_putboxpart (x+ 2 - i, y + i * (h / 4), cw, h/4,
				   bw, h, font_map, offset, i * (h / 4));
		}
	      else
		gl_putboxpart (x, y, cw, h, bw, h, font_map, offset, 0);
	    }
	  else /* char not available */
	    gl_fillbox (x, y, cw, h, current_font_fg);
	}
      else
	break;
      x += cw + pw;
      s++;
    }
}

/* log - RCS
 * $Log: gem_text.c,v $
 * Revision 1.1.1.1  2004/07/21 14:43:21  fmw
 * Imported sources
 *
 * Revision 1.3  1998/01/12 16:59:20  fmw
 * bolface and italics added
 *
 * Revision 1.2  1997/12/22 13:32:11  fmw
 * justified output improved: padding is only inserted BETWEEN characters
 * now, if character padding is on, and the heuristics for using the
 * character padding were improved, too.
 *
 * Revision 1.1  1997/12/22 10:28:44  fmw
 * Initial revision
 *
 */
