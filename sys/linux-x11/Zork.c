/* Zork.c - Data structures and support routines for the Zork widget
   */

#include <stdio.h>
#include <stdlib.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/keysym.h>
#include "ZorkP.h"

void set_mdtbl (int x, int y, int b); /* in ztex11.c */

int z_to_iso1[256] = {
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 
  0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 
  0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 
  0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 
  0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 
  0x3C, 0x3D, 0x3E, 0x3F, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 
  0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 
  0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F, 
  0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 
  0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 
  0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xE4, 
  0xF6, 0xFC, 0xC4, 0xD6, 0xDC, 0xDF, 0xAB, 0xBB, 0xEB, 0xEF, 0xFF, 0xCB, 
  0xCF, 0xE1, 0xE9, 0xED, 0xF3, 0xFA, 0xFD, 0xC1, 0xC9, 0xCD, 0xD3, 0xDA, 
  0xDD, 0xE0, 0xE8, 0xEC, 0xF2, 0xF9, 0xC0, 0xC8, 0xCC, 0xD2, 0xD9, 0xE2, 
  0xEA, 0xEE, 0xF4, 0xFB, 0xC2, 0xCA, 0xCE, 0xD4, 0xDB, 0xE5, 0xC5, 0xF8, 
  0xD8, 0xE3, 0xF1, 0xF5, 0xC3, 0xD1, 0xD5, 0xE6, 0xC6, 0xE7, 0xC7, 0xFE, 
  0xF0, 0xDE, 0xD0, 0xA3, 0x153, 0x152, 0xA1, 0xBF, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 
  0x20, 0x20, 0x20, 0x20
};

#define MAX_KEY_STRING 64

static XtResource resources[] = {
#define offset(field) XtOffset(ZorkWidget, zork.field)
  /* {name, class, type, size, offset, default_type, default_addr}, */
  { ZtNrows, ZtCRows, XtRDimension, sizeof(Dimension),
    offset(rows), XtRImmediate, (XtPointer) 21},
  { ZtNcolumns, ZtCColumns, XtRDimension, sizeof(Dimension),
    offset(columns), XtRImmediate, (XtPointer) 80},
  { XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
    offset(foreground), XtRString, XtDefaultForeground },
  
  { ZtNblack, XtCColor, XtRPixel, sizeof(Pixel),
    offset(black), XtRString, "black"},
  { ZtNred, XtCColor, XtRPixel, sizeof(Pixel),
    offset(red), XtRString, "red" },
  { ZtNgreen, XtCColor, XtRPixel, sizeof(Pixel),
    offset(green), XtRString, "green" },
  { ZtNyellow, XtCColor, XtRPixel, sizeof(Pixel),
    offset(yellow), XtRString, "yellow" },
  { ZtNblue, XtCColor, XtRPixel, sizeof(Pixel),
    offset(blue), XtRString, "blue" },
  { ZtNmagenta, XtCColor, XtRPixel, sizeof(Pixel),
    offset(magenta), XtRString, "magenta" },
  { ZtNcyan, XtCColor, XtRPixel, sizeof(Pixel),
    offset(cyan), XtRString, "cyan" },
  { ZtNwhite, XtCColor, XtRPixel, sizeof(Pixel),
    offset(white), XtRString, "white" },

#if 1  
  { ZtNfnt1, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1), XtRString, "-adobe-helvetica-medium-r-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1b, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1b), XtRString, "-adobe-helvetica-bold-r-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1i, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1i), XtRString, "-adobe-helvetica-medium-o-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1bi, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1bi), XtRString, "-adobe-helvetica-bold-o-normal--12-*-*-*-p-*-iso8859-1"},
#else
  { ZtNfnt1, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1), XtRString, "-adobe-new century schoolbook-medium-r-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1b, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1b), XtRString, "-adobe-new century schoolbook-bold-r-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1i, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1i), XtRString, "-adobe-new century schoolbook-medium-i-normal--12-*-*-*-p-*-iso8859-1"},
  { ZtNfnt1bi, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt1bi), XtRString, "-adobe-new century schoolbook-bold-i-normal--12-*-*-*-p-*-iso8859-1"},
#endif
  { ZtNfnt3, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
#if 0
    offset(fnt3), XtRString, "-adobe-courier-medium-r-normal--12-*-*-*-m-*-iso8859-1"},
#else
    offset(fnt3), XtRString, "bzork"},
#endif
  { ZtNfnt4, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt4), XtRString, "-adobe-courier-medium-r-normal--13-*-*-*-m-*-iso8859-1"},
  { ZtNfnt4b, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt4b), XtRString, "-adobe-courier-bold-r-normal--13-*-*-*-m-*-iso8859-1"},
  { ZtNfnt4i, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt4i), XtRString, "-adobe-courier-medium-o-normal--13-*-*-*-m-*-iso8859-1"},
  { ZtNfnt4bi, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(fnt4bi), XtRString, "-adobe-courier-bold-o-normal--13-*-*-*-m-*-iso8859-1"},

  { XtNcallback, XtCCallback, XtRCallback, sizeof(XtCallbackList),
    offset(input_callback), XtRCallback, (char *)0 },
#undef offset
};

static void
no_op (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
}

/* write_zork_buffer - add a Z-mapped input character to the input queue - this should be a callback! */

#define ZORK_BUFFER_MAX 100
static int zork_buffer[ZORK_BUFFER_MAX];
static int zork_buffer_lim = 0;

void
write_zork_buffer (int c)
{
  if (zork_buffer_lim < ZORK_BUFFER_MAX)
    zork_buffer[zork_buffer_lim++] = c;
}

/* read_zork_buffer - retrieve a typed z-character in `c'. If the queue is
   empty, return 0, else 1 */

int
read_zork_buffer (int *c)
{
  int i;
  if (zork_buffer_lim)
    {
      *c = zork_buffer[0];
      for (i = 1; i < zork_buffer_lim; i++)
	zork_buffer[i-1] = zork_buffer[i];
      zork_buffer_lim--;
      return 1;
    }
  return 0;
}

/* zork_input - action procedure for keyboard and mouse input */

static void
zork_input (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  XButtonEvent *button;
  XKeyEvent *key;
  KeySym key_sym;
  char key_string[MAX_KEY_STRING];
  int n;
  switch (event->type)
    {
    case ButtonPress:
    case ButtonRelease:
      button = (XButtonEvent *) event;
      if (num_params && *num_params == 1 && *params[0] == '2')
	write_zork_buffer (253); /* double click */
      else
	write_zork_buffer (254); /* single click */
      set_mdtbl (button->x, button->y,
                 event->type == ButtonPress ? button->button : -button->button);
      break;
    case KeyPress:
      key = (XKeyEvent *) event;
      n = XLookupString (key, key_string, MAX_KEY_STRING, &key_sym, NULL);
      switch (key_sym)
	{
	case XK_BackSpace:
	case XK_Delete:
	  write_zork_buffer (8); /* internal use & BUREAUCRACY */
	  break;
	case XK_Tab:
	  write_zork_buffer (32);
	  break;
	case XK_Linefeed:
	case XK_Return:
	case XK_KP_Enter:
	  write_zork_buffer (13); /* STD 0.2, � 10.7; but BUREAUCRACY wants this */
	  break;
	case XK_Escape:
	  write_zork_buffer (27);
	  break;
	  /* STD 0.2, � 10.7.2 */
	case XK_Up:
	  write_zork_buffer (129);
	  break;
	case XK_Down:
	  write_zork_buffer (130);
	  break;
	case XK_Left:
	  write_zork_buffer (131);
	  break;
	case XK_Right:
	  write_zork_buffer (132);
	  break;

	case XK_F1:
	  write_zork_buffer (133);
	  break;
	case XK_F2:
	  write_zork_buffer (134);
	  break;
	case XK_F3:
	  write_zork_buffer (135);
	  break;
	case XK_F4:
	  write_zork_buffer (136);
	  break;
	case XK_F5:
	  write_zork_buffer (137);
	  break;
	case XK_F6:
	  write_zork_buffer (138);
	  break;
	case XK_F7:
	  write_zork_buffer (139);
	  break;
	case XK_F8:
	  write_zork_buffer (140);
	  break;
	case XK_F9:
	  write_zork_buffer (141);
	  break;
	case XK_F10:
	  write_zork_buffer (142);
	  break;
	case XK_F11:
	  write_zork_buffer (143);
	  break;
	case XK_F12:
	  write_zork_buffer (144);
	  break;

	case XK_KP_0:
	  write_zork_buffer (145);
	  break;
	case XK_KP_1:
	  write_zork_buffer (146);
	  break;
	case XK_KP_2:
	  write_zork_buffer (147);
	  break;
	case XK_KP_3:
	  write_zork_buffer (148);
	  break;
	case XK_KP_4:
	  write_zork_buffer (149);
	  break;
	case XK_KP_5:
	  write_zork_buffer (150);
	  break;
	case XK_KP_6:
	  write_zork_buffer (151);
	  break;
	case XK_KP_7:
	  write_zork_buffer (152);
	  break;
	case XK_KP_8:
	  write_zork_buffer (153);
	  break;
	case XK_KP_9:
	  write_zork_buffer (154);
	  break;
	default:
	  if (n)
	    {
	      int i;
	      key_string[n] = '\0';
#if 0
	      fprintf (stderr, "%s\n", key_string);
#endif
	      for (i = 0; i < n; i++)
		{
		  if ((unsigned char) key_string[i] >= 32 && (unsigned char) key_string[i] < 127)
		    write_zork_buffer ((unsigned char) key_string[i]);
		  else if ((unsigned char) key_string[i] > 0xA0 && (unsigned char) key_string[i] <= 0xFF)
		    {
		      int j;
		      int c = 63; /* '?' ascii */
		      for (j = 0; j < 256; j++)
			if (z_to_iso1[j] == (unsigned char) key_string[i])
			  {
			    c = j;
			    break;
			  }
		      write_zork_buffer (c);
#if 0
		      fprintf (stderr, "%d -> %d\n", (unsigned char) key_string[i], c);
#endif
		    }
		  else
		    write_zork_buffer (32);
		}
	    }
	  break;
	}
      break;
    default:
      fprintf (stderr, "zork_input: unexpected X event, type = %d\n", (int) event->type);
      break;
    }
}

static XtActionsRec actions[] = {
  {"no-op", no_op},
  {"input", zork_input}
};

static char translations[] =
"<BtnDown>(2): input(2) \n\
 <BtnDown>: input(1) \n\
 <Key>: input() \
";

void zork_expose (Widget w, XEvent *event, String *str, Cardinal *len); /* currently in ztex11.c */

static void
Redisplay (Widget w, XEvent *event, Region region)
{
  zork_expose (w, event, NULL, NULL); /* currently in ztex11.c */
}

static void
Resize (Widget w)
{
#if 0
  XtCallCallbaxks (w, XtNresizeCallbaxk, (caddr_t) 0);
#endif
}

ZorkClassRec zorkClassRec = {
  { /* core fields */
    /* superclass		*/	(WidgetClass) &widgetClassRec,
    /* class_name		*/	"Zork",
    /* widget_size		*/	sizeof(ZorkRec),
    /* class_initialize		*/	0,
    /* class_part_initialize	*/	0,
    /* class_inited		*/	FALSE,
    /* initialize		*/	0,
    /* initialize_hook		*/	0,
    /* realize			*/	XtInheritRealize,
    /* actions			*/	actions,
    /* num_actions		*/	XtNumber(actions),
    /* resources		*/	resources,
    /* num_resources		*/	XtNumber(resources),
    /* xrm_class		*/	NULLQUARK,
    /* compress_motion		*/	TRUE,
    /* compress_exposure	*/	TRUE,
    /* compress_enterleave	*/	TRUE,
    /* visible_interest		*/	FALSE,
    /* destroy			*/	0,
    /* resize			*/	Resize,
    /* expose			*/	Redisplay,
    /* set_values		*/	0,
    /* set_values_hook		*/	0,
    /* set_values_almost	*/	XtInheritSetValuesAlmost,
    /* get_values_hook		*/	0,
    /* accept_focus		*/	0,
    /* version			*/	XtVersion,
    /* callback_private		*/	0,
    /* tm_table			*/	translations,
    /* query_geometry		*/	XtInheritQueryGeometry,
    /* display_accelerator	*/	XtInheritDisplayAccelerator,
    /* extension		*/	0
  },
  { /* window fields */
    /* empty			*/	0
  }
};

WidgetClass zorkWidgetClass = (WidgetClass) &zorkClassRec;

/* Log
   $Log: Zork.c,v $
   Revision 1.1.1.1  2004/07/21 14:43:21  fmw
   Imported sources
 */
