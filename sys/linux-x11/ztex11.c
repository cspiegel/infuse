/* ---------------------------------------------------------------------- */
/* ztex11.c - X11R6 z-term implementation. */
/* ---------------------------------------------------------------------- */

#include <stdlib.h>
#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xaw/Box.h>
#include <X11/Xaw/MenuButton.h>
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/SmeBSB.h>
#include <X11/Xaw/SmeLine.h>
#include "Zork.h" /* Zork widget */
#include "zterm.h" /* module interface */

/* ---------------------------------------------------------------------- */
/* XLIB RELATED MACROS */
/* ---------------------------------------------------------------------- */

/* DISPLAY - the display associated with the top widget. */

#define DISPLAY XtDisplay (top)

/* WINDOW - the window (aka drawable) associated with the zork widget */

#define WINDOW XtWindow (zork)

/* DEFGC - the default gc */

#define SCREEN DefaultScreen (DISPLAY)
#define DEFGC DefaultGC (DISPLAY, SCREEN)
#define COLORMAP DefaultColorMap (DISPLAY, SCREEN)

/* DEPTH - */

#define DEPTH DefaultDepth (DISPLAY, SCREEN)

/* ---------------------------------------------------------------------- */
/* GLOBAL VARIABLES                                                       */
/* ---------------------------------------------------------------------- */

char *zbanne = "Linux/X11"; /* machine-dependent part of banner */
int ztever = 1;             /* version number of this z-term */
int zw_inu = 0;             /* terminal width in units */
int zh_inu = 0;             /* terminal height in units */
int zmwinu = 0;             /* monospace font width in units */
int zfhinu = 0;             /* font height in units */
int zhassn = 0;             /* flag: sound effects possible */
int zhaspi = 1;             /* flag: pix didplay possible */
int zhasco = 1;             /* flag: colours available */
int zhasmo = 1;             /* flag: mouse available */
int zhasmn = 0;             /* flag: menus available */
int zhastm = 1;             /* flag: timed input available */
int zf1pro = 1;             /* flag: font 1 is proportionally spaced */
int zhasf2 = 0;             /* flag: font 2 available */
int zhasf3 = 1;             /* flag: font 3 available */
int zhasf4 = 1;             /* flag: font 4 available */
int zstyle = Z_REVERSE | Z_BOLD | Z_ITALIC | Z_FIXED; /* bitmap of available text styles */
void (*zucmou) (int x, int y, int b, int m, int e) = NULL;

static XtAppContext app_cont; /* application context */
static Widget top;            /* top-level widget */
static Widget topbox;
static Widget menubar;
static Widget zork;
static Widget desk_title;
static Widget file_title;
static Widget edit_title;
static Widget menu;
static Widget about_entry;
static Widget save_entry;
static Widget restore_entry;
static Widget restart_entry;
static Widget file_line1;
static Widget script_entry;
static Widget file_line2;
static Widget quit_entry;
static Widget undo_entry;
static Widget copy_entry;
static Widget paste_entry;
static Widget clear_entry;

static String fallback_resources[] = {
  "Infuse.geometry: 650x540", /* 640x(480+20) */
  "Infuse.maxHeight: 500",
  "Infuse.minHeight: 500",
  "Infuse.maxWidth: 660",
  "Infuse.minWidth: 660",
  "*menubar.borderWidth: 0",
  "*menubar.width: 660",
  "*menubar.height: 16",
#if 0
  "*MenuButton.width: 60",
  "*MenuButton.height: 20",
#endif
  "*MenuButton.borderWidth: 0",
  "*desk.label: Desk",
  "*desk.menu.about.label: About Infuse",
  "*file.label: File",
  "*file.menu.save.label: Save",
  "*file.menu.restore.label: Restore",
  "*file.menu.restart.label: Restart",
  "*file.menu.script.label: Script",
  "*file.menu.quit.label: Quit",
  "*edit.label: Edit",
  "*edit.menu.undo.label: Undo",
  "*edit.menu.copy.label: Copy",
  "*edit.menu.paste.label: Paste",
  "*edit.menu.clear.label: Clear",
  "*cursorName: top_left_arrow",
  "*zork.width: 640",
  "*zork.height: 400",
  NULL
};

static XFontStruct *x_fonts[16] = {
  NULL, NULL, NULL, NULL, /* f1r, f1b, f1i, f1bi */
  NULL, NULL, NULL, NULL, /* f2r, f2b, f2i, f2bi */
  NULL, NULL, NULL, NULL, /* f3r, f3b, f3i, f3bi */
  NULL, NULL, NULL, NULL  /* f4r, f4b, f4i, f4bi */
};

static Pixel x_pixels[10]; /* 2 ... 9 */
static Pixel ega_pixels[32];

/* text cursor management */

#define CRSR_W 2 /* width in pixels */
#define CRSR_T 5 /* blink period in ds */
static int crsr_countdown = CRSR_T;
static int crsr_vis = 0; /* visibility */
static int crsr_on = 0; /* flash state */
static int crsr_x = 0; /* 0-based */
static int crsr_y = 0; /* 0-based */
static int crsr_fg = Z_BLUE;
static int crsr_bg = Z_CYAN;

static long *zork_countdown = NULL;

/* pixmap - pseudo backing store for the zork widget */

static Pixmap pixmap;

/* ---------------------------------------------------------------------- */
/* PROTOTYPES                                                             */
/* ---------------------------------------------------------------------- */

static void quit_proc (Widget w, XtPointer lvl, XtPointer dat); /* menu callback */
static XFontStruct *this_font (int z_font, int z_style);
static Pixel get_pixel_resource (Widget w, char *rsrc);
static XFontStruct *get_font_resource (Widget w, char *rsrc);
void write_zork_buffer (int c); /* currently in Zork.c */
int read_zork_buffer (int *c);  /* currently in Zork.c */
extern int z_to_iso1[]; /* not really a prototype... (in Zork.c) */

static void timeout_callback (XtPointer client_data, XtIntervalId *timer);
Boolean nhApproxColor(Screen *screen, Colormap colormap, char *str, XColor *color);

/* ---------------------------------------------------------------------- */

#if 0 /* from pix2gif */
static unsigned char ega_colourmap[32][3] = {
        { 255,255,255 }, /* white */
        {   0,  0,  0 }, /* black */
        {   0,  0,  0 }, /* black */
        {   0,  0,170 }, /* blue4 (approx) */
        {   0,170,  0 }, /* green4 */
        {   0,170,170 }, /* cyan4 */
        { 170,  0,  0 }, /* red4 */
        { 170,  0,170 }, /* magenta4 */
        { 170,170,  0 }, /* yellow4 */
        { 170,170,170 }, /* grey53 */
        {  85, 85, 85 }, /* grey34 */
        {  85, 85,255 }, /* SlateBlue */
        {  85,255, 85 }, /* pale green */
        {  85,255,255 }, /* light cyan */
        { 255, 85, 85 }, /* tomato */
        { 255, 85,255 }, /* PaleVioletRed1 */
        { 255,255, 85 }, /* ivory */
        { 255,255,255 }, /* white */
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 },
        {   0,  0,  0 }};
#endif

static char *ZtNcolors[32] = {
  "white",
  "black",
  "black",
  "blue4",
  "green4",
  "cyan4",
  "red4",
  "magenta4",
  "yellow4",
  "grey53",
  "grey34",
  "Slateblue",
  "pale green",
  "light cyan",
  "tomato",
  "PaleVioletRed1",
  "ivory",
  "white",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "black"
};

/* ---------------------------------------------------------------------- */
/* INITIALIZATION & SHUTDOWN */
/* ---------------------------------------------------------------------- */

/* z_init - start up X */

void
z_init (int *pargc, char *argv[])
{
  int i;
  /* install default language procedure for all app contexts */
  XtSetLanguageProc (NULL, NULL, NULL);
  /* initialize the toolkit and application context, open display */
  top = XtVaOpenApplication (&app_cont, "Infuse", NULL, 0, pargc, argv, fallback_resources,
			     sessionShellWidgetClass, NULL);
  if (!top)
    exit (EXIT_FAILURE);

  /* create widgets */
  topbox = XtVaCreateManagedWidget ("topbox", boxWidgetClass, top, NULL);
  menubar = XtVaCreateManagedWidget ("menubar", boxWidgetClass, topbox, NULL);
  zork = XtVaCreateManagedWidget ("zork", zorkWidgetClass, topbox, NULL);

  desk_title = XtVaCreateManagedWidget ("desk", menuButtonWidgetClass, menubar, NULL);
  menu = XtVaCreatePopupShell ("menu", simpleMenuWidgetClass, desk_title, NULL);
  about_entry = XtVaCreateManagedWidget ("about", smeBSBObjectClass, menu, NULL);

  file_title = XtVaCreateManagedWidget ("file", menuButtonWidgetClass, menubar, NULL);
  menu = XtVaCreatePopupShell ("menu", simpleMenuWidgetClass, file_title, NULL);
  save_entry = XtVaCreateManagedWidget ("save", smeBSBObjectClass, menu, NULL);
  restore_entry = XtVaCreateManagedWidget ("restore", smeBSBObjectClass, menu, NULL);
  restart_entry = XtVaCreateManagedWidget ("restart", smeBSBObjectClass, menu, NULL);
  file_line1=XtVaCreateManagedWidget("file_line1",smeLineObjectClass,menu,NULL);
  script_entry = XtVaCreateManagedWidget ("script", smeBSBObjectClass, menu, NULL);
  file_line2=XtVaCreateManagedWidget("file_line2",smeLineObjectClass,menu,NULL);
  quit_entry = XtVaCreateManagedWidget ("quit", smeBSBObjectClass, menu, NULL);

  edit_title = XtVaCreateManagedWidget ("edit", menuButtonWidgetClass, menubar, NULL);
  menu = XtVaCreatePopupShell ("menu", simpleMenuWidgetClass, edit_title, NULL);
  undo_entry = XtVaCreateManagedWidget ("undo", smeBSBObjectClass, menu, NULL);
  copy_entry = XtVaCreateManagedWidget ("copy", smeBSBObjectClass, menu, NULL);
  paste_entry = XtVaCreateManagedWidget ("paste", smeBSBObjectClass, menu, NULL);
  clear_entry = XtVaCreateManagedWidget ("clear", smeBSBObjectClass, menu, NULL);

  /* prepare callbacks */
  XtAddCallback (about_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (save_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (restore_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (restart_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (script_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (quit_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (undo_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (copy_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (paste_entry, XtNcallback, quit_proc, NULL);
  XtAddCallback (clear_entry, XtNcallback, quit_proc, NULL);

  /* init widgets, map window */
  XtRealizeWidget (top);

  /* init fonts */
  {
    int i;
    /* font 1 */
    x_fonts[0] = get_font_resource (zork, ZtNfnt1);
    x_fonts[1] = get_font_resource (zork, ZtNfnt1b);
    x_fonts[2] = get_font_resource (zork, ZtNfnt1i);
    x_fonts[3] = get_font_resource (zork, ZtNfnt1bi);
    /* font 2 */
    x_fonts[4] = x_fonts[0];
    x_fonts[5] = x_fonts[0];
    x_fonts[6] = x_fonts[0];
    x_fonts[7] = x_fonts[0];
    /* font 3 */
    x_fonts[8] = get_font_resource (zork, ZtNfnt3);
    x_fonts[9] = x_fonts[8];
    x_fonts[10] = x_fonts[8];
    x_fonts[11] = x_fonts[8];
    /* font 4 */
    x_fonts[12] = get_font_resource (zork, ZtNfnt4);
    x_fonts[13] = get_font_resource (zork, ZtNfnt4b);
    x_fonts[14] = get_font_resource (zork, ZtNfnt4i);
    x_fonts[15] = get_font_resource (zork, ZtNfnt4bi);
    /* sanity check */
    for (i = 0; i < 16; i++)
      if (x_fonts[i] == NULL)
	{
	  fprintf (stderr, "font %d (style %d) is NULL\n", i / 4, i % 4);
	  exit (EXIT_FAILURE);
	}
  }

  /* init text color pixels */
  x_pixels[Z_BLACK] = get_pixel_resource (zork, ZtNblack);
  x_pixels[Z_RED] = get_pixel_resource (zork, ZtNred);
  x_pixels[Z_GREEN] = get_pixel_resource (zork, ZtNgreen);
  x_pixels[Z_YELLOW] = get_pixel_resource (zork, ZtNyellow);
  x_pixels[Z_BLUE] = get_pixel_resource (zork, ZtNblue);
  x_pixels[Z_MAGENTA] = get_pixel_resource (zork, ZtNmagenta);
  x_pixels[Z_CYAN] = get_pixel_resource (zork, ZtNcyan);
  x_pixels[Z_WHITE] = get_pixel_resource (zork, ZtNwhite);

  /* init EGA palette for picture display */
  for (i = 0; i < 32; i++)
    {
      XColor tmp;
      nhApproxColor (ScreenOfDisplay (DISPLAY, SCREEN), DefaultColormap (DISPLAY, SCREEN), ZtNcolors[i], &tmp);
      ega_pixels[i] = tmp.pixel;
    }

  /* init pseudo-backing-store pixmap */
  pixmap = XCreatePixmap (DISPLAY, WINDOW, 640, 400, DEPTH);
  XSetForeground (DISPLAY, DEFGC, x_pixels[Z_BLUE]);
  XFillRectangle (DISPLAY, pixmap, DEFGC, 0, 0, 640, 400);

  /* init global configuration variables */
  zw_inu = 640;
  zh_inu = 400;
  zmwinu = x_fonts[12]->max_bounds.width;
  zfhinu = x_fonts[12]->ascent + x_fonts[12]->descent;
  zf1pro = 1;

  /* init timer callback - called 10 times per second */
  XtAppAddTimeOut (app_cont, 100, timeout_callback, NULL);
}

/* z-exit - exit ZTERM */

void
z_exit (void)
{
}

/* ---------------------------------------------------------------------- */
/* THE SCROLLING REGION                                                   */
/* ---------------------------------------------------------------------- */

/* scroll_top - the first line to copy in zscrol() */

static int scroll_top = 0;

/* scroll_bot - the lat line to copy in zscrol() */

static int scroll_bot = 400-1;

/* zsescr - set scrolling region (for zscrol).  `top' is the first */
/* line which scrolls, and `bot' is first one which doesn't.  Since */
/* z-coordinates are 1-based, we subtract 1 from `top' and 2 from */
/* `bot' (2, since the `bot' line doesn't scroll). */

void
zsescr (int top, int bot)
{
  scroll_top = top - 1;
  scroll_bot = bot - 2;
}

/* zscrol - scroll up the scroll region (set with zsescr) by n */
/* lines. */
#if 0
void
zscrol (int n)
{
#if 0
  XCopyArea (DISPLAY, pixmap, WINDOW, DEFGC,
	     0, scroll_top + n, 640, scroll_bot - (scroll_top + n) + 1,
	     0, scroll_top);
  XCopyArea (DISPLAY, pixmap, pixmap, DEFGC,
	     0, scroll_top + n, 640, scroll_bot - (scroll_top + n) + 1,
	     0, scroll_top);
#else
  zcpbox (1, scroll_top + n + 1, 640, scroll_bot - (scroll_top + n) + 2,
	  1, scroll_top + 1);
#endif
}
#endif
/* ---------------------------------------------------------------------- */
/* BOX / PIX OPERATIONS                                                   */
/* ---------------------------------------------------------------------- */

void
zcpbox (int x, int y, int w, int h, int x1, int y1)
{
  XCopyArea (DISPLAY, pixmap, WINDOW, DEFGC, x - 1, y - 1, w, h, x1 - 1, y1 - 1);
  XCopyArea (DISPLAY, pixmap, pixmap, DEFGC, x - 1, y - 1, w, h, x1 - 1, y1 - 1);
}

void
zshbox (int x, int y, int w, int h, int c)
{
  XSetForeground (DISPLAY, DEFGC, x_pixels[c]);
  XFillRectangle (DISPLAY, pixmap, DEFGC, x-1, y-1, w, h);  
  XFillRectangle (DISPLAY, WINDOW, DEFGC, x-1, y-1, w, h);  
}

void 
zshpix (int x, int y, int w, int h, void *img)
{
#if 0
  zshbox (x, y, w, h, Z_GREEN);
#else
  int xi, eta;
  unsigned char *data = (unsigned char *) img;
  XSetState (DISPLAY, DEFGC, x_pixels[Z_MAGENTA], x_pixels[Z_CYAN], GXcopy, AllPlanes);
  for (eta = 0; eta < h; eta++)
    {
      for (xi = 0; xi < w; xi++)
	{
#if 1
	  XSetForeground (DISPLAY, DEFGC, ega_pixels[data[w * eta + xi] & 0x1F]); /* mod 32 */
#else
	  XSetForeground (DISPLAY, DEFGC, ega_pixels[(w * eta + xi) & 0x1F]); /* mod 32 */
#endif
	  XDrawPoint (DISPLAY, pixmap, DEFGC, x + xi - 1, y + eta - 1);
	}
    }
  XCopyArea (DISPLAY, pixmap, WINDOW, DEFGC, x - 1, y - 1, w, h, x - 1, y - 1);
#endif
}

/*
  GC components
  function
  plane-mask
  foreground
  subwindow-mode
  clip-x
  clip-y
  clip-mask
  */

/* ---------------------------------------------------------------------- */
/* CURSOR OPERATIONS                                                      */
/* ---------------------------------------------------------------------- */

static void
erase_crsr (void)
{
  if (crsr_vis && crsr_on)
    {
      XSetForeground (DISPLAY, DEFGC, x_pixels[crsr_bg]);
      XFillRectangle (DISPLAY, pixmap, DEFGC, crsr_x, crsr_y, CRSR_W, zfhinu);
      XFillRectangle (DISPLAY, WINDOW, DEFGC, crsr_x, crsr_y, CRSR_W, zfhinu);
      crsr_on = 0;
    }
}

static void
draw_crsr (void)
{
  if (crsr_vis)
    {
      XSetForeground (DISPLAY, DEFGC, x_pixels[crsr_fg]);
      XFillRectangle (DISPLAY, pixmap, DEFGC, crsr_x, crsr_y, CRSR_W, zfhinu);
      XFillRectangle (DISPLAY, WINDOW, DEFGC, crsr_x, crsr_y, CRSR_W, zfhinu);
      crsr_on = 1;
    }
}

/* zshcur - show the cursor.  The coordinates are, just like string */
/* coordinates, the upper left corner.  The color should be the current */
/* foreground color. This function also installs a timer interrupt which */
/* flashes the cursor while it is visible. */

void
zshcur (int x, int y, int fg, int bg)
{
  if (crsr_vis)
    zhdcur ();
  crsr_x = x - 1;
  crsr_y = y - 1;
  crsr_fg = fg;
  crsr_bg = bg;
  crsr_vis = 1;
  draw_crsr ();
}

/* zhdcur - hide the cursor.  The color should be the current background */
/* color. */

void
zhdcur (void)
{
  erase_crsr ();
  crsr_vis = 0;
}

/* ---------------------------------------------------------------------- */
/* STRING DISPLAY                                                         */
/* ---------------------------------------------------------------------- */

/* zshstr - draw a string of z-glyphs. The x and y coordinates refer to */
/* the upper left corner of the bounding box of the string, while the X */
/* text drawing functions expect the coordinates of the base line.  See */
/* p.173 of the Xlib manual. For XDrawImageString, see p.177.  GCs are */
/* discussed on pp.127 */

#define MAX_W_BUF 1024
static char w_buf[MAX_W_BUF];

void
zshstr (int x, int y, int w, int align, z_glyph *s, int n)
{
  int i, j, dx = 0, dy;
  XFontStruct *font;
  x--;
  y--;
  /* process the REVERSE style */
  for (i = 0; i < n; i++)
    {
      if (s[i].style & Z_REVERSE)
	{
	  unsigned tmp;
	  tmp = s[i].fg;
	  s[i].fg = s[i].bg;
	  s[i].bg = tmp;
          s[i].style &= ~Z_REVERSE;
	}
    }
  for (i = 0; i < n; i += j)
    {
      w_buf[0] = (unsigned char) z_to_iso1[s[i].ch];
      for (j = 1; j < MAX_W_BUF && i + j < n; j++)
	{
	  if (s[i].font != s[i+j].font || s[i].style != s[i+j].style
	      || s[i].fg != s[i+j].fg || s[i].bg != s[i+j].bg)
	    break;
	  w_buf[j] = (unsigned char) z_to_iso1[s[i+j].ch];
	}
      XSetForeground (DISPLAY, DEFGC, x_pixels[s[i].fg]);
      XSetBackground (DISPLAY, DEFGC, x_pixels[s[i].bg]);
      font = this_font (s[i].font, s[i].style);
      dy = font->ascent + font->descent;
      XSetFont (DISPLAY, DEFGC, font->fid);
      XDrawImageString (DISPLAY, WINDOW, DEFGC, x + dx, y + dy, w_buf, j);
      XDrawImageString (DISPLAY, pixmap, DEFGC, x + dx, y + dy, w_buf, j);
      dx += XTextWidth (this_font (s[i].font, s[i].style), w_buf, j);
    }
}

/* ---------------------------------------------------------------------- */
/* FONT METRICS                                                           */
/* ---------------------------------------------------------------------- */

/* zstrwd - return the width of a string of z-glyphs.  We do this by */
/* adding up the width of individual substrings consisting of characters */
/* with identical characteristics. */

int
zstrwd (z_glyph *s, int n)
{
  int i, j, w = 0;  
  for (i = 0; i < n; i += j)
    {
      w_buf[0] = (unsigned char) z_to_iso1[s[i].ch];
      for (j = 1; j < MAX_W_BUF && i + j < n; j++)
	{
	  if (s[i].font != s[i+j].font || s[i].style != s[i+j].style
	      || s[i].fg != s[i+j].fg || s[i].bg != s[i+j].bg)
	    break;
	  w_buf[j] = (unsigned char) z_to_iso1[s[i+j].ch];
	}
      w += XTextWidth (this_font (s[i].font, s[i].style), w_buf, j);
    }
  return w;
}

/* ---------------------------------------------------------------------- */
/* INPUT                                                                  */
/* ---------------------------------------------------------------------- */

/* zrdchr - read a character from the keyboard, or return if the timeout */
/* expires.  This is a replacement for XtAppMainLoop() - see �7.6 (p.119) */
/* of the Intrinsics manual.  (XtAppLock() is cargo cult... only useful */
/* in multi-threaded environment?)  The wait_ds parameter contains the */
/* amount of time to wait before returning a 0. If keyboard input */
/* occurred before the time-out, the remainig time is stored in wait_ds, */
/* and the time-out is destroyed. Thus another call to zrdchr() (without */
/* changing wait_ds) will simply make the time-out simply continue from */
/* where it left off (not counting any delay between the two calls to */
/* zrdchr, however). */

int
zrdchr (long *wait_ds)
{
  int ret;
  XEvent event;
  XtAppLock (app_cont);
#if 1
  if (wait_ds && *wait_ds == -1) /* sadly, calls with -1 still proliferate */
    {
      printf ("ERROR: zrdchr called with -1\n");
      exit (EXIT_FAILURE);
    }
#endif
  zork_countdown = wait_ds;
  do
    {
      XtAppNextEvent (app_cont, &event);
      XtDispatchEvent (&event);
    }
  while (!read_zork_buffer (&ret));
  XtAppUnlock (app_cont);
  return ret;
}

/* ---------------------------------------------------------------------- */
/* KLUDGES                                                                */
/* ---------------------------------------------------------------------- */

void
zflush (void)
{
}

/* ---------------------------------------------------------------------- */
/* UTILITY FUNCTIONS                                                      */
/* ---------------------------------------------------------------------- */

/* this_font - return the font struct corresponding to a z-font-style     */
/* combination.  The `reverse' attribute is not taken into account here.  */
/* The `fixed' attribute causes font-1 to be mapped to font-4, since      */
/* fonts 3 and 4 are fixed already.  Font 3 is only available in          */
/* roman. Thus we get nine fonts; mapped to the following indices:        */
/*   1r 1b 1i 1bi ->  0, 1, 2, 3                                          */
/*   3r           ->  8                                                   */
/*   4r 4b 4i 4bi -> 12,13,14,15                                          */

static XFontStruct *
this_font (int z_font, int z_style)
{
  int style = 0, font = 0;
  z_style &= zstyle;
  if (z_style & Z_BOLD)
    style += 1;
  if (z_style & Z_ITALIC)
    style += 2;
  if (z_font == Z_FONT1)
    {
      if ((z_style & Z_FIXED) && zf1pro && zhasf4)
	font = 3;
    }
  else if (z_font == Z_FONT3 && zhasf3)
    {
      font = 2;
      style = 0;
    }
  else if (z_font == Z_FONT4 && zhasf4)
    font = 3;
  return x_fonts[4 * font + style];
}

/* get_pixel_resource - return the pixel value stored in the widget's */
/* resources. */

static Pixel
get_pixel_resource (Widget w, char *rsrc)
{
  Arg arg[1];
  Pixel pixel;
  XtSetArg (arg[0], rsrc, &pixel);
  XtGetValues (w, arg, 1);
  return pixel;
}

/* ###################################################################### */
/* from nethack-3.2.1/win/X11/winX.c                                      */

/*
 * Find a color that approximates the color named in "str".  The "str" color
 * may be a color name ("red") or number ("#7f0000").  If str == NULL, then
 * "color" is assumed to contain the RGB color wanted.
 * The approximate color found is returned in color as well.
 * Return True if something close was found.
 */
Boolean
nhApproxColor(Screen *screen, Colormap colormap, char *str, XColor *color)
#if 0
Screen	 *screen;	/* screen to use */
Colormap colormap;	/* the colormap to use */
char     *str;		/* color name */
XColor   *color;	/* the X color structure; changed only if successful */
#endif
{
    int		ncells;
    long	cdiff = 16777216; /* 2^24; hopefully our map is smaller */
    XColor	tmp;
    static	XColor *table = 0;
    register	i, j;
    register long tdiff;

    /* if the screen doesn't have a big colormap, don't waste our time */
    /* or if it's huge, and _some_ match should have been possible */
    if((ncells = CellsOfScreen (screen)) < 256 || ncells > 4096)
	return False;

    if (str != (char *)0) {
	if (!XParseColor(DisplayOfScreen(screen), colormap, str, &tmp))
	    return False;
    } else {
	tmp = *color;
	tmp.flags = 7;	/* force to use all 3 of RGB */
    }

    if (!table) {
	table = (XColor *) XtCalloc(ncells, sizeof(XColor));
	for(i=0; i<ncells; i++)
	    table[i].pixel = i;
	XQueryColors(DisplayOfScreen(screen), colormap, table, ncells);
    }

    /* go thru cells and look for the one with smallest diff */
    /* diff is calculated abs(reddiff)+abs(greendiff)+abs(bluediff) */
    /* a more knowledgeable color person might improve this -dlc */
try_again:
    for(i=0; i<ncells; i++) {
	if(table[i].flags == tmp.flags) {
	    j = (int)table[i].red - (int)tmp.red;
	    if(j < 0) j = -j;
	    tdiff = j;
	    j = (int)table[i].green - (int)tmp.green;
	    if(j < 0) j = -j;
	    tdiff += j;
	    j = (int)table[i].blue - (int)tmp.blue;
	    if(j < 0) j = -j;
	    tdiff += j;
	    if(tdiff < cdiff) {
		cdiff = tdiff;
		tmp.pixel = i; /* table[i].pixel == i */
	    }
	}
    }

    if(cdiff == 16777216) return False;	/* nothing found?! */

    /*
     * Found something.  Return it and mark this color as used to avoid
     * reuse.  Reuse causes major contrast problems :-)
     */
    *color = table[tmp.pixel];
    table[tmp.pixel].flags = 0;
    /* try to alloc the color, so no one else can change it */
    if(!XAllocColor(DisplayOfScreen(screen), colormap, color)) {
	cdiff = 16777216;
	goto try_again;
    }
    return True;
}

/* ###################################################################### */

/* set_mtdbl - call the zucmou up-call. b is negative if */
/* released. Menus not implemented. Buttons flaky (because RELEASE is */
/* not reported here. */

void
set_mdtbl (int x, int y, int b)
{
  static int buttons = 0;
  if (b < -3)
    b = -3;
  if (b > 3)
    b = 3;
  if (b < 0)
    buttons &= ~(1 << (3 - b));
  else
    buttons |= 1 << (3 - b);
  if (zucmou != NULL)
    zucmou (x, y, buttons, 0, 0);
}

/* get_font_resource - return the font value stored in the widget's */
/* resources. */

static XFontStruct *
get_font_resource (Widget w, char *rsrc)
{
  Arg arg[1];
  XFontStruct *font;
  XtSetArg (arg[0], rsrc, &font);
  XtGetValues (w, arg, 1);
  return font;
}

/* ---------------------------------------------------------------------- */
/* CALLBACK PROCEDURES                                                    */
/* ---------------------------------------------------------------------- */

/* quit_proc - callback for menu entries */

static void
quit_proc (Widget w, XtPointer lvl, XtPointer dat)
{
  write_zork_buffer (252); /* menu click */
}

/* timeout_callback - callback for input timer events.  We have to make */
/* the function XNextEvent() return, (because it doesn't for timer */
/* events), so we send a little bogus message. */

static void
timeout_callback (XtPointer client_data, XtIntervalId *timer)
{
  XClientMessageEvent ev;
  if (zork_countdown != NULL)
    {
      (*zork_countdown)--;
      if (*zork_countdown <= 0)
	{
#if 0
	  printf ("zork timeout\n");
#endif
          *zork_countdown = 0;
          zork_countdown = NULL;
	  write_zork_buffer (0);
	  ev.type = ClientMessage;
	  ev.format = 8;
	  strcpy (ev.data.b, "ZORK TIMEOUT");
	  XSendEvent (DISPLAY, InputFocus, FALSE, 0, (XEvent *) &ev);
	}
    }
  crsr_countdown--;
  if (crsr_countdown <= 0)
    {
#if 0
      printf ("cursor timeout\n");
#endif
      crsr_countdown = CRSR_T;
      if (crsr_on)
	erase_crsr ();
      else
	draw_crsr ();      
    }
  XtAppAddTimeOut (app_cont, 100, timeout_callback, NULL);
}

/* zork_expose - not really a callback, but rather an action procedure. */

void
zork_expose (Widget w, XEvent *event, String *str, Cardinal *len)
{
  XCopyArea(DISPLAY, pixmap, XtWindow(w), DEFGC,
	    event->xexpose.x, event->xexpose.y,
	    event->xexpose.width, event->xexpose.height,
	    event->xexpose.x, event->xexpose.y);  
}

/* ---------------------------------------------------------------------- *
 * $Log: ztex11.c,v $
 * Revision 1.1.1.1  2004/07/21 14:43:21  fmw
 * Imported sources
 *
 * ---------------------------------------------------------------------- */
