/* ----------------------------------------------------------------------
   testit.c - test suite for zterms
   ---------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "zterm.h"

#define MAX_BUF 1023
static char buf[MAX_BUF+1];
static z_glyph zbuf[MAX_BUF+1];

static int
zprintf (int x, int y, const char *fmt, ...)
{
  va_list ap;
  int i, n;
  va_start (ap, fmt);
  n = vsnprintf (buf, MAX_BUF, fmt, ap);
  for (i = 0; buf[i]; i++)
    {
      zbuf[i].ch = (unsigned char) buf[i];
      zbuf[i].font = Z_FONT4;
      zbuf[i].style = Z_ROMAN|Z_ITALIC;
      zbuf[i].fg = Z_CYAN;
      zbuf[i].bg = Z_BLUE;
    }
  zshstr (x, y, zstrwd (zbuf, i), Z_LEFT, zbuf, i);
  va_end (ap);
  return n;
}

static void
test_colors (void)
{
  int fg;
  int bg;
  for (fg = Z_BLACK; fg <= Z_WHITE; fg++)
    {
      for (bg = Z_BLACK; bg <= Z_WHITE; bg++)
	{
	  zbuf[bg - Z_BLACK].ch = '#';
	  zbuf[bg - Z_BLACK].font = Z_FONT1;
	  zbuf[bg - Z_BLACK].style = Z_ROMAN|Z_BOLD;
	  zbuf[bg - Z_BLACK].fg = fg;
	  zbuf[bg - Z_BLACK].bg = bg;
	}
      zshstr (10, 50+(fg-Z_BLACK)*16, 0, Z_LEFT, zbuf, Z_WHITE-Z_BLACK+1);
    }
}

static void
test_lines (void)
{
  int l, n;
  n = zh_inu / zfhinu;
  for (l = 0; l < n; l++)
    zprintf (1, l*zfhinu+1, "line %d - the quick brown fox jumps over the lazy dog", l+1);
}

int
main (int argc, char *argv[])
{
  int c;
  z_init (&argc, argv);
  do
    {
      c = zrdchr (0);
      switch (c)
	{
	case 0:
	  zprintf (0, 0, "Timeout !");
	  break;
	case 8:
	  zprintf (30, 30, "back-space");
	  test_lines ();
	  break;
	case 13:
	  zprintf (30, 30, "(new-line) -- width %d, height %d, fntw %d, fnth %d", zw_inu, zh_inu, zmwinu, zfhinu);
	  break;
	case 27:
	  zprintf (30, 30, "escape");
	  test_colors ();
	  break;
	case 129:
	  zprintf (30, 30, "cursor up");
	  break;
	case 130:
	  zprintf (30, 30, "cursor down");
	  break;
	case 131:
	  zprintf (30, 30, "cursor left");
	  break;
	case 132:
	  zprintf (30, 30, "cursor right");
	  break;
	default:
	  if (c >= 32 && c < 127 || c >= 155 && c <= 251)
	    zprintf (30, 30, "letter `%c'", c);
	  else if (c >= 133 && c < 145)
	    zprintf (30, 30, "f%d", c - 132);
	  else if (c >= 145 && c < 155)
	    zprintf (30, 30, "keypad %d", c - 145);
	  else
	    zprintf (30, 30, "code %d", c);
	  break;
	}
    }
  while (c != 'q' && c != 'Q');
  z_exit ();
  return 0;
}

/* ----------------------------------------------------------------------
   $Log: testit.c,v $
   Revision 1.1.1.1  2004/07/21 14:43:21  fmw
   Imported sources

   ---------------------------------------------------------------------- */
