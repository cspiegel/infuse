/* ztncur.c - LINUX/NCURSES z-term
   
   This file is part of Infuse
   
   INFUSE COPYRIGHT
   ================
   Infuse is a generic z-machine emulator.
   
   Copyright (C) 1997 - 2019 by Florian M. Weps.
 
      Florian M. Weps
      Dieffenbachstr. 36
      10967 Berlin
      Germany
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of Version 3 of the GNU General Public License as
   published by the Free Software Foundation.
   
   This program is distributed WITHOUT ANY WARRANTY; without even the
   implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
   PURPOSE.  See Version 3 of the GNU General Public License for more
   details.
   
   You should have received a copy of Version 3 of the GNU General Public
   License along with this program. If not, you can FTP the license from
   prep.ai.mit.edu/pub/gnu/COPYING-3 or write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#define _XOPEN_SOURCE_EXTENDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strerror() */
#include <wchar.h>
#include <locale.h>
#include <ncursesw/curses.h>
#include "zterm.h"

#if defined WA_ITALIC
#define ZWA_ITALIC WA_ITALIC
#else
#define ZWA_ITALIC WA_UNDERLINE
#endif

/* ncurses V1 mouse support autoconf */
#if defined (NCURSES_MOUSE_VERSION) && (NCURSES_MOUSE_VERSION == 1)
#define ZMOUSE 1
#else
#define ZMOUSE 0
#endif

char *zbanne = "Linux/ncurses"; /* machine-dependent part of banner */
int ztever = 1;  /* version number of this z-term */
int zw_inu = 0;  /* terminal width in units */
int zh_inu = 0;  /* terminal height in units */
int zmwinu = 1;  /* monospace font width in units */
int zfhinu = 1;  /* font height in units */
int zhassn = 0;  /* flag: sound effects possible */
int zhaspi = 0;  /* flag: pix display possible */
int zhasco = 0;  /* flag: colours available */
int zhasmo = 0;  /* flag: mouse available */
int zhasmn = 0;  /* flag: menus available */
int zhastm = 1;  /* flag: timed input available */
int zf1pro = 0;  /* flag: font 1 is proportionally spaced */
int zhasf2 = 0;  /* flag: font 2 available */
int zhasf3 = 1;  /* flag: font 3 available */
int zhasf4 = 1;  /* flag: font 4 available */
int zstyle = (Z_ROMAN | Z_BOLD | Z_ITALIC | Z_REVERSE | Z_FIXED);

static cchar_t *outchs;


static wchar_t *z_to_unicode[256] = {
  L"\x00", L"\x01", L"\x02", L"\x03", L"\x04", L"\x05", L"\x06", L"\x07", L"\x08", L"\x09", L"\x0A", L"\x0B", 
  L"\x0C", L"\x0D", L"\x0E", L"\x0F", L"\x10", L"\x11", L"\x12", L"\x13", L"\x14", L"\x15", L"\x16", L"\x17", 
  L"\x18", L"\x19", L"\x1A", L"\x1B", L"\x1C", L"\x1D", L"\x1E", L"\x1F", L"\x20", L"\x21", L"\x22", L"\x23", 
  L"\x24", L"\x25", L"\x26", L"\x27", L"\x28", L"\x29", L"\x2A", L"\x2B", L"\x2C", L"\x2D", L"\x2E", L"\x2F", 
  L"\x30", L"\x31", L"\x32", L"\x33", L"\x34", L"\x35", L"\x36", L"\x37", L"\x38", L"\x39", L"\x3A", L"\x3B", 
  L"\x3C", L"\x3D", L"\x3E", L"\x3F", L"\x40", L"\x41", L"\x42", L"\x43", L"\x44", L"\x45", L"\x46", L"\x47", 
  L"\x48", L"\x49", L"\x4A", L"\x4B", L"\x4C", L"\x4D", L"\x4E", L"\x4F", L"\x50", L"\x51", L"\x52", L"\x53", 
  L"\x54", L"\x55", L"\x56", L"\x57", L"\x58", L"\x59", L"\x5A", L"\x5B", L"\x5C", L"\x5D", L"\x5E", L"\x5F", 
  L"\x60", L"\x61", L"\x62", L"\x63", L"\x64", L"\x65", L"\x66", L"\x67", L"\x68", L"\x69", L"\x6A", L"\x6B", 
  L"\x6C", L"\x6D", L"\x6E", L"\x6F", L"\x70", L"\x71", L"\x72", L"\x73", L"\x74", L"\x75", L"\x76", L"\x77", 
  L"\x78", L"\x79", L"\x7A", L"\x7B", L"\x7C", L"\x7D", L"\x7E", L"\x7F", L"\x20", L"\x20", L"\x20", L"\x20", 
  L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", 
  L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\xE4", 
  L"\xF6", L"\xFC", L"\xC4", L"\xD6", L"\xDC", L"\xDF", L"\xBB", L"\xAB", L"\xEB", L"\xEF", L"\xFF", L"\xCB", 
  L"\xCF", L"\xE1", L"\xE9", L"\xED", L"\xF3", L"\xFA", L"\xFD", L"\xC1", L"\xC9", L"\xCD", L"\xD3", L"\xDA", 
  L"\xDD", L"\xE0", L"\xE8", L"\xEC", L"\xF2", L"\xF9", L"\xC0", L"\xC8", L"\xCC", L"\xD2", L"\xD9", L"\xE2", 
  L"\xEA", L"\xEE", L"\xF4", L"\xFB", L"\xC2", L"\xCA", L"\xCE", L"\xD4", L"\xDB", L"\xE5", L"\xC5", L"\xF8", 
  L"\xD8", L"\xE3", L"\xF1", L"\xF5", L"\xC3", L"\xD1", L"\xD5", L"\xE6", L"\xC6", L"\xE7", L"\xC7", L"\xFE", 
  L"\xF0", L"\xDE", L"\xD0", L"\xA3", L"\u0153", L"\u0152", L"\xA1", L"\xBF", L"\x20", L"\x20", L"\x20", L"\x20", 
  L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", 
  L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", L"\x20", 
  L"\x20", L"\x20", L"\x20", L"\x20"
};

static wchar_t *z_font3_to_unicode[128] = {
  L"\x00", L"\x01", L"\x02", L"\x03", L"\x04", L"\x05", L"\x06", L"\x07", /* 00 .. 07 */
  L"\x08", L"\x09", L"\x0A", L"\x0B", L"\x0C", L"\x0D", L"\x0E", L"\x0F", /* 08 .. 0F */
  L"\x10", L"\x11", L"\x12", L"\x13", L"\x14", L"\x15", L"\x16", L"\x17", /* 10 .. 17 */
  L"\x18", L"\x19", L"\x1A", L"\x1B", L"\x1C", L"\x1D", L"\x1E", L"\x1F", /* 18 .. 1F */
  L"\x20", L"\u2190", L"\u2192", L"\x2571", L"\x2572", L"\x20", L"\x2500", L"\x2500",     /* 20 .. 27 " ←→╱╲ ──" */
  L"\x2502", L"\x2502", L"\x2534", L"\x252C", L"\x251C", L"\x2524", L"\x2514", L"\x250C", /* 28 .. 2F "││┴┬├┤└┌" */ 
  L"\x2510", L"\x2518", L"\x2514", L"\x250C", L"\x2510", L"\x2518", L"\x2588", L"\x2580", /* 30 .. 38 "┐┘└┌┐┘█▀" */
  L"\x2584", L"\x258C", L"\x2590", L"\x2584", L"\x2580", L"\x258C", L"\x2590", L"\x259D", /* 38 .. 3F "▄▌▐▄▀▋▐▝" */
  L"\x2597", L"\x2596", L"\x2598", L"\x259D", L"\x2597", L"\x2596", L"\x2598", L"\x20",   /* 40 .. 48 "▗▖▘▝▗▖▘▔" */
  L"\x20", L"\x20", L"\x20", L"\x2594", L"\x2581", L"\x258F", L"\x2595", L"\x20",         /* 49 .. 4F "   ▔▁▏▕ " */
  L"\x258F", L"\x258E", L"\x258D", L"\x258C", L"\x258B", L"\x258A", L"\x2589", L"\x2588", /* 50 .. 58 "▏▎▍▌▋▊▉█" */
  L"\x2595", L"\x258F", L"\x2573", L"\x253C", L"\x2191", L"\x2193", L"\x2195", L"\x2395", /* 59 .. 5F "▕▏╳┼↑↓↕⎕" */
  L"\x3F", L"\x16AA", L"\x16D2", L"\x16C7", L"\x16DE", L"\x16D6", L"\x16A0", L"\x16B7",   /* 60 .. 68 "?ᚪᛒᛇᛞᛖᚠᚷ" */
  L"\x16BA", L"\x16C1", L"\x16C4", L"\x16E3", L"\x16DA", L"\x16D7", L"\x16BE", L"\x16A9", /* 69 .. 6F "ᚺᛁᛄᛣᛚᛗᚾᚩ" */
  L"\x16C8", L"\x16B3", L"\x16B1", L"\x16CB", L"\x16CF", L"\x16A2", L"\x16E0", L"\x16B9", /* 70 .. 78 "ᛈᚳᚱᛋᛏᚢᛠᚹ" */
  L"\x16C9", L"\x16A3", L"\x16DF", L"\x2191", L"\x2193", L"\x2195", L"\x3F", L"\x20"      /* 79 .. 7F "ᛉᚣᛟ↑↓↕? " */
};

static int z_colors[10] = {0, 0,
			   COLOR_BLACK, COLOR_RED, COLOR_GREEN, COLOR_YELLOW,
			   COLOR_BLUE, COLOR_MAGENTA, COLOR_CYAN, COLOR_WHITE};

void (*zucmou) (int x, int y, int b, int m, int e); /* mouse up-call */

void
z_init (int *pargc, char *argv[])
{
  setlocale(LC_CTYPE, "");
  initscr ();
  cbreak ();
  noecho ();
  nonl ();
  intrflush (stdscr, FALSE);
  keypad (stdscr, TRUE);
  zw_inu = COLS;
  zh_inu = LINES;
  if (has_colors () == TRUE) {
    int i, j;
    zhasco = 1;
    start_color ();
    for (i = 0; i < 8; i++) {
      for (j = 0; j < 8; j++) {
	init_pair (i * 8 + j, j, i);
      }
    }
  }
#if ZMOUSE
  if (mousemask (ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION,
		 NULL) != 0) {
    zhasmo = 1;
  }
#endif
  outchs = (cchar_t *) malloc (COLS * sizeof (*outchs));
  if (outchs == NULL) {
    endwin ();
    fprintf (stderr, "virtual memory exhausted\n");
    exit (EXIT_FAILURE);
  }  
}

void
z_exit (void)
{
  endwin ();
}

void
zsescr (int top, int bot)
{
  /* not used in core 1.57 */
}

/* zscrol - scroll up n units in scroll region */

void
zscrol (int n)
{
  /* not used in core 1.57 */
}

void
zcpbox (int x, int y, int w, int h, int x1, int y1)
{
  copywin (stdscr, stdscr, y-1, x-1, y1-1, x1-1, y1-1 + h-1, x1-1 + w-1,
	   FALSE);
}

void
zshbox (int x, int y, int w, int h, int c)
{
  int line, col;
  for (col = 0; col < w; col++) {
    setcchar (outchs + col, L" ", WA_NORMAL, 8 * z_colors[c], NULL);
  }
  for (line = y-1; line < y + h - 1; line++) {
    mvadd_wchnstr (line, x-1, outchs, w);
  }
}

void
zshpix (int x, int y, int w, int h, void *img)
{
  /* not supported in text mode */
}

void
zshcur (int x, int y, int fg, int bg)
{
  move (y - 1, x - 1);  
}

void
zhdcur (void)
{
  /* can't do that / not necessary in text mode */
}

/* zshstr - draw string at position. The alignment parameter align is
   not used in core 1.57 */

void
zshstr (int x, int y, int w, int align, z_glyph *s, int n)
{
  int i;
  wchar_t t[2];
  t[1] = L'\0';
  if (w < n) {
    n = w; /* paranoia */
  }
  for (i = 0; i < n; i++) {
    if (Z_FONT3 == s[i].font) {
      if (s[i].ch >= 123) {
	s[i].style ^= Z_REVERSE;
      }
      setcchar (outchs + i, z_font3_to_unicode[s[i].ch % 128], 
	WA_NORMAL
	| ((s[i].style & Z_REVERSE) ? WA_REVERSE : 0)
	| ((s[i].style & Z_BOLD) ? WA_BOLD : 0)
	| ((s[i].style & Z_ITALIC) ? ZWA_ITALIC : 0),
	8 * z_colors[s[i].bg] + z_colors[s[i].fg],
	NULL);
    } else {
      t[0] = s[i].ch;
      setcchar (outchs + i, t, 
	WA_NORMAL
	| ((s[i].style & Z_REVERSE) ? WA_REVERSE : 0)
	| ((s[i].style & Z_BOLD) ? WA_BOLD : 0)
	| ((s[i].style & Z_ITALIC) ? ZWA_ITALIC : 0),
	8 * z_colors[s[i].bg] + z_colors[s[i].fg],
	NULL);
    }
  }
  mvadd_wchnstr (y-1, x-1, outchs, n);
}

int
zstrwd (z_glyph *s, int n)
{
  return n;
}

int
zrdchr (long *wait_ms)
{
  wint_t c;
  int status, i;
  int ms_to_wait = wait_ms ? (int) (*wait_ms) : 0;
  unsigned char b;
try_again:
  //ms_to_wait /= 100;
  if (ms_to_wait > 0)
    {
      do
	{
	  halfdelay (ms_to_wait > 255 ? 255 : ms_to_wait);
	  status = get_wch (&c);
	  nocbreak (); /* leave half-delay mode */
	  cbreak ();
	  ms_to_wait -= 255;
	}
      while (status == ERR && ms_to_wait > 0);
    }
  else
    status = get_wch (&c);
  b = 0x20; /* default value */
  if (ERR == status) {
    /* timeout */
    b = 0;
  } else if (KEY_CODE_YES == status) {
    switch (c) {
      case KEY_BACKSPACE:
      case KEY_DC: /* delete character */
	b = Z_BS; /* signal BS - only key not in table of §10.7 */
	break;
      case KEY_ENTER:
	b = Z_CR; /* LF is erroneously recommended in §10.7 */
	break;
      case KEY_DOWN:
	b = Z_CRSR_DOWN;
	break;
      case KEY_UP:
	b = Z_CRSR_UP;
	break;
      case KEY_LEFT:
	b = Z_CRSR_LEFT;
	break;
      case KEY_RIGHT:
	b = Z_CRSR_RIGHT;
	break;
      case KEY_F(1):
      case KEY_F(2):
      case KEY_F(3):
      case KEY_F(4):
      case KEY_F(5):
      case KEY_F(6):
      case KEY_F(7):
      case KEY_F(8):
      case KEY_F(9):
      case KEY_F(10):
      case KEY_F(11):
      case KEY_F(12):
	b = Z_F1 + (c - KEY_F(1));
	break;
      case KEY_IC: /* keypad 0 */
	b = Z_KP0;
	break;
      case KEY_C1: /* keypad 1 */
	b = Z_KP1;
	break;
      case KEY_C3: /* keypad 3 */
	b = Z_KP3;
	break;
      case KEY_B2: /* keypad 5 */
	b = Z_KP5;
	break;
      case KEY_A1: /* keypad 7 */
	b = Z_KP7;
	break;
      case KEY_A3: /* keypad 9 */
	b = Z_KP9;
	break;
#if ZMOUSE
      case KEY_MOUSE:
	{
	  MEVENT mevent;
	  int buttons;
	  getmouse (&mevent);
	  if ((mevent.bstate & BUTTON1_CLICKED)
	      || (mevent.bstate & BUTTON2_CLICKED)
	      || (mevent.bstate & BUTTON3_CLICKED)
	      || (mevent.bstate & BUTTON4_CLICKED)) {
	    b = Z_SINGLE_CLICK;
	  } else if ((mevent.bstate & BUTTON1_DOUBLE_CLICKED)
	      || (mevent.bstate & BUTTON2_DOUBLE_CLICKED)
	      || (mevent.bstate & BUTTON3_DOUBLE_CLICKED)
	      || (mevent.bstate & BUTTON4_DOUBLE_CLICKED)) {
	    b = Z_DOUBLE_CLICK;
	  }
	  if (mevent.bstate & BUTTON1_PRESSED) buttons |= 1;
	  if (mevent.bstate & BUTTON2_PRESSED) buttons |= 2;
	  if (mevent.bstate & BUTTON3_PRESSED) buttons |= 4;
	  if (mevent.bstate & BUTTON4_PRESSED) buttons |= 8;
	  if (zucmou) zucmou (mevent.x+1, mevent.y+1, buttons, 0, 0);
	}
      if (b == 0X20) goto try_again;
      break;
#endif
    } 
  } else if (OK == status) {
    switch (c)
      {
      case 27: /* ESC */
	b = Z_ESC;
	break;
      case 8:
      case 127:
	b = Z_BS; /* signal BS - only key not in table of §10.7 */
	break;
      case 10:
      case 13:
	b = Z_CR; /* LF is erroneously recommended in §10.7 */
	break;
      default:
	b = 0x20;
	if (c > 0x20 && c < 0x7F) {
	  /* printable ascii */
	  b = c;
	} else if (c > 0xA0) {
	  /* printable UNICODE */
	  for (i = 155; i <= 251; i++) {
	    if (cunitt[i - 155] == c) {
	      /* search UNICODE code in x-lation table */
	      b = i;
	      break;
	    }
	  }
	}
	break;
      }
  }
  return b;
}
  
void
zflush (void)
{
  refresh ();
}

