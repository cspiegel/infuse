INFUSE - a Generic Z-Machine Emulator
=====================================

Infuse is an interpreter for Z-Code games, such as the historic
[Infocom](http://www.infocom-if.org/) games.  It can run games of version 3, 4,
5, 6, 7, and 8.  V6 support is not complete.

Development started in 1997, based on the "Specification of the Z-machine,
Standard Version 0.2" document of Nov. 15, 1995. In 2017, work started to
update Infuse to conform to [draft standard
1.2](http://curiousdannii.github.io/if/zspec12.html).

The interpreter consists of a large core module containing the Z-machine
emulator, and a suite of small machine-dependent modules for various operating
systems and user interfaces.

The machine-dependent modules only have to provide about 10 functions and some
integer configuration variables to the core module.  The Infuse core has been
written to support both text-mode screens with only one font and a single
style, as well as bit-mapped color screens with proportionally spaced fonts and
many styles.

As of version 0.95, there are three such modules, all for the Linux operating
system:

* a simple stdio module
* curses console, uses unicode to simulate the *Beyond Zork* font
* X11 using the Athena Widget set, very buggy (but it has a complete *Beyond
  Zork* font).

Features
--------

Infuse has the following features:

* Standard 1.2 (WIP)
* Z-Machine V3-8 (V6 is WIP)
* heuristics for selecting the correct interpreter number for canonical games
  such as *Beyond Zork*
* the character graphics font used in *Beyond Zork* (approximated in curses and
  stdio modules, full font in X11)
* unicode (in curses and stdio modules)
* sound (WIP)
* color support (true color is WIP)
* all text styles (curses and X11)
* timed input in the X11 and curses modules
* undo (only one level currently)
* *Blorb* story files as well as bare ones
* *Quetzal* save file format
* a vanity extended opcode to enable stories to print the interpreter version
  string
* profiling and debugging opcodes from
  [Zoom](https://www.logicalshift.co.uk/mac/zoom/manual/)

Future Roadmap
--------------

Infuse aims to be a reference implementation for Z-machine Standard 1.2,
regarding both correctness and feature-completeness.

In addition, the roadmap includes:

* [Glk](https://eblong.com/zarf/glk/) module, making Infuse portable to all
  platforms for which this toolkit is available
* modern X11 module, perhaps using the GTK toolkit
* comprehensive automated test suite, suitable for CI

Custom Extended Op-Codes
------------------------

#### EXT:128 START\_TIMER

* Start an internal timer measuring the amount of CPU time spent in
  microseconds
* Use gestalt selector $1000 to check availability.

#### EXT:129 STOP\_TIMER

* Stop the timer
* Use gestalt selector $1000 to check availability.

#### EXT:130 PRINT\_TIMER

* Print the time elapsed between the last start/stop timer invocations.
* Use gestalt selector $1000 to check availability.

#### EXT:131 READ\_TIMER -> seconds

* Store the number of seconds elapsed between the last start/stop timer
  invocations.
* Use gestalt selector $1000 to check availability.

#### EXT:132 DUMP\_STACK

* Print out a stack dump.
* Use gestalt selector $1001 to check availability.

#### EXT:198 PRINT\_VERSION max\_chars

* Print interpreter version string. If the optional parameter n is specified,
  not more than *max\_chars* characters are printed. This is useful if the
  output should be captured with output stream 3
* Use gestalt selector $FE86 to check availability.

Reporting a Bug
---------------

You can report bugs by [opening an issue in
gitlab](https://gitlab.com/monkeymind/infuse/issues) or [via
mail](mailto:monkeymind@hactrn.ch).

